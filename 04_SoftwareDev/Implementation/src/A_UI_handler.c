#pragma mul

#include "A_UI_handler.h"
#include "M_led_handler.h" 
#include "M_eeprom_handler.h" 
#include "H_lcd_driver.h"
#include "H_keypad_driver.h"
#include "stdlib.h"
#include "string.h" 
#include "M_power_handler.h"
#include "M_ZONE_intr_hdlr.h"
#include "M_mode_key_handler.h" 
#include "M_GSM_interface_hdlr.h"

/******************************************DEFNITIONS_START***************************************/
//#define PWD_DEFAULT 1

//keyinput defnitions   

#define PLAY_CALL_TIMEOUT 150
#define PLAY_TIME_MIN 2//IN 500ms units
#define PLAY_TIME_MAX 10//IN 500ms units

#define UI_IDLE_TIME_MAX 40
#define SEL_LEFT 1
#define SEL_RIGHT 8
#define MENU_INDEX_OFFSET 1 //ex:SELF_DIA - MENU_INDEX_OFFSET = s8_G_display_menu_index

#define MAX_ZONE_DELAY 99 //in seconds
#define MAX_SIREN_TIME 99//in minutes

//INPUT MAX LEN

#define MAX_DELAY_COUNT 5

#define PH_BOOK_LEN 7
#define ZONE_NO_PRINT_LEN 2
#define DEFAULT_PRINT_LEN 4

#define EDIT_MODE 1
#define VIEW_MODE 0

//length defInitions
//#define DISP_MENU_LEN 9
#define DISP_MENU_LEN 8
 


#define MAX_RESP_TIME 40
#define MAX_REC_TIME 100

#define MAX_CALL_TIME 200

#define ZONE_CONF_LEN 3

#define PH_NO_LIST_LEN 10 

#define G_ACT_ISO_LEN 2
#define MODE_SET_LEN 3

/******************************************DEFNITIONS_END***************************************/
/******************************************VARIABLES_START***************************************/
//const char s8_G_display_menu[DISP_MENU_LEN][16]={"PLAY SETTINGS  ","PH.NO CONFIG   ","ZONE SETTINGS  ","SIREN SETTINGS ","RECORD VOICE   ","PLAY CALL      ","CHANGE PWD     ","MODE CONTROL   ","SELF_DIAGNOSTIC"};
//const char s8_G_display_menu[DISP_MENU_LEN][16]={"PH.NO CONFIG   ","ZONE SETTINGS  ","SIREN SETTINGS ","RECORD VOICE   ","PLAY CALL      ","CHANGE PWD     ","TAKE LOG    ","MODE CONTROL   ","SELF_DIAGNOSTIC"};
const char s8_G_display_menu[DISP_MENU_LEN][16]={"PH.NO CONFIG   ","ZONE SETTINGS  ","SIREN SETTINGS ","RECORD VOICE   ","PLAY CALL      ","CHANGE PWD     ","TAKE LOG    ","MODE CONTROL   "};
const char s8_G_zone_config_options[ZONE_CONF_LEN][16] = {"ACT/ISO        ","TRIG SETTINGS  ","E_DELAY        "};
const char s8_G_mode_setting_options[3][16] = {"DISARM   ","PART ARM  ","ARM"};
const char s8_G_phone_book[10][PH_BOOK_LEN] = {"A1 NO","A2 NO","A3 NO","A4 NO","A5 NO","A6 NO","A7 NO","A8 NO","A9 NO","A10 NO"}; 


const char s8_G_active_isolate[2][3]={"I","A"};
const char s8_G_act_iso[G_ACT_ISO_LEN][4]={"ISO","ACT"};
const char s8_G_NO_NC[G_NO_NC_LEN][5]={"NO","NC"}; 
const char s8_G_arm_parm[4][MODE_STR_LEN]={"24","PA","A","E"};

static uint8_t u8frag_cnt = 0;
uint8_t u8UI_timer_cnt = 0;
/**************NEWLY ADDED 19-9-2018*****************/
int8_t s8LCD_disp_index = 0;
uint8_t u8LCD_nxt_disp_index = 1;
/**************NEWLY ADDED 19-9-2018*****************/

int16_t s16_UI_IdleTimeout = UI_IDLE_TIME_MAX;

//user-input variable
char s8_G_usr_inp[MAX_LCD_TXT_LEN]={0};
uint8_t u8_G_usr_inp_ind=0;

//LINEAR TEMP ARRAY


//NOTIFICATION VARIABLES
char *LCD_ROW1_MSG = NULL;
char *LCD_ROW2_MSG = NULL;
uint8_t NXT_STATE=0;//VALUE LESS THAN 255
int16_t delay_count = 0;

//ZONE/PH_NO SELECTION VARIABLES
uint8_t u8_inp_choice = VIEW_MODE;

/******************************************VARIABLES_END***************************************/
/******************************************STRUCTURES_START***************************************/
 
 


typedef struct 
{	
	char number[PHONE_NO_OFFSET];
	ACTIVE_STS no_state;
	
}NO_SETTINGS;


NO_SETTINGS ph_no[10] = {{"8072500974",ISOLATE},{"9176731172",ISOLATE},{"8072500974",ISOLATE}};




SYSTEM_SETTINGS sys_data = {1,10,ON,EXT_KEY,DEFAULT_MODE};//AT INIT THE MODE MUST BE DEFAULT MODE
/******************************************STRUCTURES_END***************************************/
/******************************************ENUM_START***************************************/
typedef enum
{				//NO VALUES OF ENUM MUST BE CHANGED
	MENU_STATE = 0,   	
	PH_NO_CONFIG,
	ZONE_SETTING_CONF,
	SIREN_SETTINGS,
	RECORD_VOICE,
	PLAY_CALL,	
	CHANGE_PWD,
	TAKE_LOG,
	MODE_CTRL_CONFIG,
	SELF_DIA,
	SHOW_MSG=100,
	SAVE_SETTINGS=101,
	RESP_WAIT = 102,
	UI_EXIT_STATE = 103,
	PLAY_SETTINGS = 104,
}MAIN_STATE_T;

typedef enum
{				 //NO VALUES OF ENUM MUST BE CHANGED
	PLAY_SETTING_OPTIONS = PLAY_SETTINGS * 10,
	PLAY_ZONE_SETTINGS,
	PLAY_PHONE_NO,
	PHONE_NO_CONFIG_HOME=PH_NO_CONFIG * 10,
	GET_NUMBERS,
	SET_NUMBERS,
	ZONE_SELECT = ZONE_SETTING_CONF * 10,
	ZONE_CONF,
	ZONE_STATE_CONF,
	ZONE_TRIG_CONF,
	ZONE_E_DELAY_CONF,
	ZONE_MODE_CONF,
	ZONE_ENTER_CHECK,
	SIREN_OPTIONS = SIREN_SETTINGS * 10,
	SIREN_STATE,
	SIREN_TIME,
	RECORD_VOICE_HOME = RECORD_VOICE * 10,
	RECORDING,
	RESPONSE_WAIT,
	PLAY_CALL_HOME = PLAY_CALL * 10,
	CALL_WAIT,
	CMDRESP_WAIT,
	VALIDATE = CHANGE_PWD * 10,
	GET_NEW_PWD,	
	LOG_HOME = TAKE_LOG * 10,
	LOG_START_STATE,
	LOG_DELETE_STATE,
	LOG_IN_PRO_STATE,
	LOG_DEL_IN_PRO_STATE,
	MODE_CTRL_OPTIONS = MODE_CTRL_CONFIG * 10,
	MODE_CONTROL,
	MODE_CONFIG,
	SELF_DIA_HOME = SELF_DIA * 10,
	SELF_DIA_WAIT,
}SUB_STATE_T;

//state variables
MAIN_STATE_T e_G_main_state=MENU_STATE;
SUB_STATE_T e_G_sub_state=VALIDATE;
/******************************************ENUM_END***************************************/

/******************************************FUNCTION_DECLARATIONS_START***************************************/
uint8_t first_digit(uint8_t num);
void show_msg(char *string1,char *string2,uint8_t next_state,uint8_t delay_time);
//void v_arr_store_char(uint16_t index,uint16_t offset,char *buff);
void v_arr_store_char(uint8_t index,char *buff);
void m_UI_MainState_enter(MAIN_STATE_T curr_state);
void m_UI_PreCond_hdlr(MAIN_STATE_T curr_state);
//void v_cpy_str(uint8_t row,uint8_t column,char *buff);
 uint8_t u8_get_zone_dly(uint8_t inp_len, char **usr_inp,char input);
/******************************************FUNCTION_DECLARATION_END***************************************/

uint8_t a_vUI_handler(char input)
{
	G_RESP_CODE ret;	 
	uint8_t G_ret = WAITING;	 
	uint16_t num;
	char *inp_ptr = NULL;	 
	if('\0' == input)	 
	{
		s16_UI_IdleTimeout--;
		if(s16_UI_IdleTimeout <= 0)
		{
			/*G_ret = EXIT;
			e_G_main_state = MENU_STATE;
			s8LCD_disp_index  =0;	
			u8LCD_nxt_disp_index = 1;
			lcd_clear_screen();*/
			s16_UI_IdleTimeout = UI_IDLE_TIME_MAX;
		}
	}
	else
	{
		s16_UI_IdleTimeout = UI_IDLE_TIME_MAX;
	}
	
	switch(e_G_main_state)
	{
		case MENU_STATE:
		{		
			if(input != 0)
			{							
				if(input == UP) 
				{
					s8LCD_disp_index--;
					if(s8LCD_disp_index < 0)
					{
						s8LCD_disp_index = DISP_MENU_LEN-1;
					}
					s8LCD_disp_index = s8LCD_disp_index % DISP_MENU_LEN;
					u8LCD_nxt_disp_index = (s8LCD_disp_index + 1)% DISP_MENU_LEN;
					m_UI_PreCond_hdlr(MENU_STATE);
				}
				else if(input == DOWN)
				{
					s8LCD_disp_index++;
					s8LCD_disp_index = s8LCD_disp_index % DISP_MENU_LEN;
					u8LCD_nxt_disp_index = (s8LCD_disp_index + 1)% DISP_MENU_LEN;
					m_UI_PreCond_hdlr(MENU_STATE);
				}
				else if(input == ENTER)
				{
					e_G_main_state = (s8LCD_disp_index+1);
					//ensure value must not exceed 255 since it is a u8 type
					e_G_sub_state = e_G_main_state * 10; 				
					s8LCD_disp_index = 0;					 
					u8LCD_nxt_disp_index = 1;
					//lcd_clear_screen();	
					m_UI_PreCond_hdlr(e_G_main_state);
					
				}	
				else if(input == BACK)
				{
					e_G_main_state = SAVE_SETTINGS;
					lcd_clear_screen();
					s8LCD_disp_index = 0 + SEL_LEFT;
					m_UI_PreCond_hdlr(SAVE_SETTINGS);
				}				
			}		
			break;
		}
		case PLAY_SETTINGS:
		{	switch(e_G_sub_state)
			{
				case PLAY_SETTING_OPTIONS:
				{					
					if(input != 0)
					{							
						if(input == UP) 
						{
							s8LCD_disp_index=0;
							m_UI_PreCond_hdlr(PLAY_SETTINGS);
								
						}
						else if(input == DOWN)
						{
							s8LCD_disp_index=1;
							m_UI_PreCond_hdlr(PLAY_SETTINGS);
						}
						else if(input == ENTER)
						{
							e_G_sub_state = PLAY_ZONE_SETTINGS + s8LCD_disp_index;
							s8LCD_disp_index = -1;
							delay_count = 0;
							//lcd_clear_screen();							//m_UI_PreCond_hdlr(PLAY_SETTINGS);
						}	
						else if(input == BACK)
						{						
							m_UI_MainState_enter(PLAY_SETTINGS);
							m_UI_PreCond_hdlr(MENU_STATE);
						}	
					}
					break;	
				}
				case PLAY_ZONE_SETTINGS:
				{	
					if(delay_count <= 0)
					{
						delay_count = PLAY_TIME_MAX;
						s8LCD_disp_index++;
						s8LCD_disp_index = s8LCD_disp_index % ZONE_COUNT;
						//lcd_clear_screen();
						m_UI_PreCond_hdlr(e_G_main_state);
					}
					delay_count--;
					
					if(input != 0)
					{
						if(input == BACK)
						{
							e_G_sub_state = PLAY_SETTING_OPTIONS;
							s8LCD_disp_index = 0;
							delay_count=0;							
							m_UI_PreCond_hdlr(e_G_main_state);
						}						 
					}				
					break;	
				}
				case PLAY_PHONE_NO:
				{
					if(delay_count <= 0)
					{						
						delay_count = PLAY_TIME_MAX;
						s8LCD_disp_index++;
						s8LCD_disp_index = s8LCD_disp_index % PH_NO_LIST_LEN;						
						if(strlen(ph_no[s8LCD_disp_index].number)<1)
						{
							delay_count = PLAY_TIME_MIN;
						}
						lcd_clear_screen();
						m_UI_PreCond_hdlr(e_G_main_state);
					}
					delay_count--;					
					if(input != 0)
					{
						if(input == BACK)
						{
							e_G_sub_state = PLAY_SETTING_OPTIONS;
							s8LCD_disp_index = 0;
							delay_count=0;
							lcd_clear_screen();
							m_UI_PreCond_hdlr(e_G_main_state);
						}						 
					}				
					break;	
				}
				default:break;
			}
			break;
		}
		case PH_NO_CONFIG:
		{
			switch(e_G_sub_state)
			{
				case PHONE_NO_CONFIG_HOME:
				{				 				
					if(input != 0)
					{							
						if(input == UP) 
						{
							s8LCD_disp_index--;
							if(s8LCD_disp_index < 0)
							{
								s8LCD_disp_index = PH_NO_LIST_LEN-1;
							}
							s8LCD_disp_index = s8LCD_disp_index % PH_NO_LIST_LEN;							 
						        m_UI_PreCond_hdlr(e_G_main_state);
						}
						else if(input == DOWN)
						{
							s8LCD_disp_index++;
							s8LCD_disp_index = s8LCD_disp_index % PH_NO_LIST_LEN;							 
						        m_UI_PreCond_hdlr(e_G_main_state);
						}
						else if(input == ENTER)
						{
							u8_inp_choice = s8LCD_disp_index;
							e_G_sub_state = GET_NUMBERS;	
							lcd_clear_screen();
							s8LCD_disp_index = 0;							 					
						        m_UI_PreCond_hdlr(e_G_main_state);
						}	
						else if(input == BACK)
						{					
							m_UI_MainState_enter(PH_NO_CONFIG);
							m_UI_PreCond_hdlr(e_G_main_state);
						}	
						else if(input == KEY_RESET)
						{
							u8_inp_choice = s8LCD_disp_index;
							e_G_sub_state = SET_NUMBERS;
							s8LCD_disp_index = 0;
							lcd_clear_screen();
							m_UI_PreCond_hdlr(e_G_main_state);
						}
					}
					break;	
				}
				case GET_NUMBERS:
				{
					ret = u8_get_input_test(PHONE_NO_OFFSET,&inp_ptr,input);//u8_get_input("ENTER PH NO    ",PHONE_NO_OFFSET,&inp_ptr,input);	
					if(ret != WAITING)
					{
						if(ret == SUCCESS)
						{							 
							v_arr_store_char(u8_inp_choice,inp_ptr);//
							clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);							 
							e_G_sub_state = SET_NUMBERS;
							s8LCD_disp_index = 0;
							lcd_clear_screen();
							m_UI_PreCond_hdlr(e_G_main_state);
						}
						else if(ret == EXIT)
						{
							e_G_sub_state = PHONE_NO_CONFIG_HOME;
							s8LCD_disp_index = u8_inp_choice;
							lcd_clear_screen();
							m_UI_PreCond_hdlr(e_G_main_state);
						}
					}											
					break;	
				}
				case SET_NUMBERS:
				{					 
					if(input != 0)
					{			
						if(input == UP) 
						{
							s8LCD_disp_index=0;	
							m_UI_PreCond_hdlr(e_G_main_state);
						}
						else if(input == DOWN)
						{
							s8LCD_disp_index=1;	
							m_UI_PreCond_hdlr(e_G_main_state);
						}
						else if(input == ENTER)
						{							 
							//EWrite_DataBuffer[PH_NO_STATUS_START_INDEX+(u8_inp_choice*PH_NO_STATUS_OFFSET)]=(uint8_t)s8LCD_disp_index;
							ph_no[u8_inp_choice].no_state = s8LCD_disp_index;
							e_G_sub_state = PHONE_NO_CONFIG_HOME;
							s8LCD_disp_index = u8_inp_choice;
							lcd_clear_screen();
							m_UI_PreCond_hdlr(e_G_main_state);
						}	
						else if(input == BACK)
						{							 
							e_G_sub_state = PHONE_NO_CONFIG_HOME;
							s8LCD_disp_index = u8_inp_choice;
							lcd_clear_screen();
							m_UI_PreCond_hdlr(e_G_main_state);
							
						}	
					}
					break;	
				}
				default:
				break;
			}
			break;
		}
		case ZONE_SETTING_CONF:
		{	
			if(input != 0)
			{			
				switch(e_G_sub_state)
				{
					case ZONE_SELECT:
					{					 
						if(input == DOWN)
						{
							s8LCD_disp_index++;
							if(s8LCD_disp_index > ZONE_COUNT-1)
							{
								s8LCD_disp_index = 0;
							}							
						}					 
						if(input == RIGHT) 
						{
							e_G_sub_state = ZONE_CONF; 							
						}
						else if(input == LEFT) 
						{							
							e_G_sub_state = ZONE_ENTER_CHECK; 
						}						 
						m_UI_PreCond_hdlr(e_G_main_state);
						break;
					}
					case ZONE_CONF:
					{						 
						if(input == DOWN)
						{
							sensor_index[s8LCD_disp_index].active_sts++;
							if(sensor_index[s8LCD_disp_index].active_sts > G_ACT_ISO_LEN-1)
							{
								sensor_index[s8LCD_disp_index].active_sts = 0;
							}
						}					 
						if(input == RIGHT) 
						{
							e_G_sub_state = ZONE_TRIG_CONF;							
						}
						else if(input == LEFT) 
						{
							e_G_sub_state = ZONE_SELECT;							
						}						 
						m_UI_PreCond_hdlr(e_G_main_state);
						break;
					}
					case ZONE_TRIG_CONF:
					{
						if(input == DOWN)  
						{
							sensor_index[s8LCD_disp_index].trig_setting++;
							if(sensor_index[s8LCD_disp_index].trig_setting > G_NO_NC_LEN-1)
							{
								sensor_index[s8LCD_disp_index].trig_setting = 0;
							}
						}					 
						else if(input == RIGHT) 
						{
							e_G_sub_state = ZONE_STATE_CONF; 
							
						}
						else if(input == LEFT) 
						{
							e_G_sub_state = ZONE_CONF;
							
						}						 
						m_UI_PreCond_hdlr(e_G_main_state);
						break;
					}
					
					case ZONE_STATE_CONF:
					{						 
						if((input == DOWN)&&(s8LCD_disp_index!=3))
						{
							sensor_index[s8LCD_disp_index].mode_setting++;
							if(sensor_index[s8LCD_disp_index].mode_setting > MODE_SET_LEN-1)
							{
								sensor_index[s8LCD_disp_index].mode_setting = 0;
							}
						}
						
						
						if(input == RIGHT) 
						{
							e_G_sub_state = ZONE_ENTER_CHECK; 
							
						}
						else if(input == LEFT) 
						{
							e_G_sub_state = ZONE_TRIG_CONF;
							
						}					 
						m_UI_PreCond_hdlr(e_G_main_state);
						break;
					}
					case ZONE_ENTER_CHECK:
					{
						if(input == ENTER)
						{
							e_G_sub_state = ZONE_E_DELAY_CONF;						 
						}
						else if(input == RIGHT) 
						{
							e_G_sub_state = ZONE_SELECT; 	
						}
						else if(input == LEFT) 
						{
							e_G_sub_state = ZONE_STATE_CONF;
								
						}
						m_UI_PreCond_hdlr(e_G_main_state);
						break;
					}
					case ZONE_E_DELAY_CONF:
					{						
						ret = u8_get_zone_dly(3,&inp_ptr,input);
						if(ret != WAITING)
						{
							if(ret == SUCCESS)
							{
								num = atoi(inp_ptr);
								if(num > MAX_ZONE_DELAY)
								{	
									
									//show_msg("  ZONE DELAY   ","   TOO LONG    ",ZONE_E_DELAY_CONF,6);
								}
								else
								{	
									sensor_index[s8LCD_disp_index].delay_timing = num;
									
								}										 
								e_G_sub_state = ZONE_ENTER_CHECK;
								m_UI_PreCond_hdlr(e_G_main_state);
								clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);
							}
							if(ret == EXIT)
							{
								e_G_sub_state = ZONE_ENTER_CHECK;
								m_UI_PreCond_hdlr(e_G_main_state);
								clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);
								
							}
						}
						input = '\0';
						break;					
					}
					default:
					break;
				}
				if(input == BACK)
				{
					m_UI_MainState_enter(ZONE_SETTING_CONF);
					m_UI_PreCond_hdlr(e_G_main_state);					 
				}
			}	 
			break;
		}
		case SIREN_SETTINGS:
		{	
			switch(e_G_sub_state)
			{
				case SIREN_OPTIONS:
				{
					/*lcd_goToXy(1,s8LCD_disp_index+1);
					lcd_printf(">");
					lcd_goToXy(1,(!s8LCD_disp_index)+1);
					lcd_printf(" ");
					lcd_goToXy(2,1);
					lcd_printf("SIREN STATE   ");
					lcd_goToXy(2,2);
					lcd_printf("SIREN TIME    ");*/
					if(input != 0)
					{							
						if(input == UP) 
						{
							s8LCD_disp_index=0;
							m_UI_PreCond_hdlr(e_G_main_state);
								
						}
						else if(input == DOWN)
						{
							s8LCD_disp_index=1;
							m_UI_PreCond_hdlr(e_G_main_state);
								
						}
						else if(input == ENTER)
						{
							e_G_sub_state = SIREN_STATE + s8LCD_disp_index;
							s8LCD_disp_index = 0;
							lcd_clear_screen();
							m_UI_PreCond_hdlr(e_G_main_state);
						}	
						else if(input == BACK)
						{
							/*e_G_main_state = MENU_STATE;
							s8LCD_disp_index = SIREN_SETTINGS - MENU_INDEX_OFFSET;
							u8LCD_nxt_disp_index = (s8LCD_disp_index + 1)% DISP_MENU_LEN;
							lcd_clear_screen();*/
							
							m_UI_MainState_enter(SIREN_SETTINGS);
							m_UI_PreCond_hdlr(e_G_main_state);
						}	
					}	
					break;
				}
				case SIREN_STATE:
				{
					/*lcd_goToXy(1,s8LCD_disp_index+1);
					lcd_printf(">");
					lcd_goToXy(1,(!s8LCD_disp_index)+1);
					lcd_printf(" ");
					lcd_goToXy(2,1);
					lcd_PrintSP_d("SIREN OFF",10);
					lcd_goToXy(2,2);
					lcd_PrintSP_d("SIREN ON",10);
					lcd_goToXy(14,(u8E2P_buff[SIREN_STATE_START_INDEX])+1);
					lcd_printf("_");*/
					if(input != 0)
					{							
						if(input == UP) 
						{
							s8LCD_disp_index=0;	
							m_UI_PreCond_hdlr(e_G_main_state);
						}
						else if(input == DOWN)
						{
							s8LCD_disp_index=1;	
							m_UI_PreCond_hdlr(e_G_main_state);
						}
						else if(input == ENTER)
						{
							sys_data.u8_G_siren_state = s8LCD_disp_index;
							//EWrite_DataBuffer[SIREN_STATE_START_INDEX]=(uint8_t)s8LCD_disp_index;
							e_G_sub_state = SIREN_OPTIONS;
							s8LCD_disp_index = 0;
							lcd_clear_screen();
							m_UI_PreCond_hdlr(e_G_main_state);
						}	
						else if(input == BACK)
						{
							e_G_sub_state = SIREN_OPTIONS;
							s8LCD_disp_index = 0;
							lcd_clear_screen();
							m_UI_PreCond_hdlr(e_G_main_state);
						}	
					}	
					break;
				}
				case SIREN_TIME:
				{
					/*lcd_goToXy(13,1);
					lcd_printnum(sys_data.u16_G_siren_time,MAX_DELAY_COUNT);
					lcd_goToXy(9,2);
					lcd_printf("in mins");*/
					ret = u8_get_input_test(MAX_DELAY_COUNT,&inp_ptr,input);//u8_get_input("ENTER TIME",MAX_DELAY_COUNT,&inp_ptr,input);	
					if(ret!= WAITING)
					{
						if(ret == SUCCESS)
						{
							num = atoi(inp_ptr);
							if(num > MAX_SIREN_TIME)
							{							 
								show_msg("  SIREN TIME   ","   TOO LONG    ",SIREN_TIME,6);
							}
							else
							{
								//EWrite_DataBuffer[SIREN_TIME_START_INDEX]=(uint8_t)((num & 0xff00)>>8);
								//EWrite_DataBuffer[SIREN_TIME_START_INDEX+1]=(uint8_t)(num & 0x00ff);
								sys_data.u16_G_siren_time = num;
								//clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);							 
								s8LCD_disp_index = 0;
								lcd_clear_screen();
								e_G_sub_state = SIREN_OPTIONS;	
								m_UI_PreCond_hdlr(e_G_main_state);
							}
							clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);
						}
						else if(ret == EXIT)
						{
							e_G_sub_state = SIREN_OPTIONS;
							s8LCD_disp_index = 1;
							lcd_clear_screen();
							m_UI_PreCond_hdlr(e_G_main_state);
						}
					}					
					break;
				}
				default:
				break;
			}
			break;	
		}
		case RECORD_VOICE:
		{	
			switch(e_G_sub_state)
			{
				case RECORD_VOICE_HOME:
				{
					if(input==RECORD)
		                        {				 
						GSM_CommandSend(E_REC_START,DEFAULT);	
						e_G_sub_state=RESPONSE_WAIT;	
						u8UI_timer_cnt = 0;
					}
					else if(input==BACK)
					{						
						m_UI_MainState_enter(RECORD_VOICE);
						m_UI_PreCond_hdlr(e_G_main_state);
					}
					break;
				}
				case RECORDING:
				{
					if(KEY_stsGet()==RELEASED)//key status getting variables have been shifted to main_app_handler
					{
						GSM_CommandSend(E_REC_STOP,DEFAULT);	
						e_G_sub_state=RESPONSE_WAIT;	
						u8UI_timer_cnt = 0;
					}
					else
					{
						u8UI_timer_cnt++;
						if(u8UI_timer_cnt > MAX_REC_TIME)
						{
							u8UI_timer_cnt =0;
							m_UI_MainState_enter(RECORD_VOICE);
							show_msg("   RECORDING   ","     ERROR     ",MENU_STATE,6);
						}
					}
					break;
				}
				case RESPONSE_WAIT:
				{
					u8UI_timer_cnt++;
					if(u8UI_timer_cnt > MAX_RESP_TIME)
					{
						u8UI_timer_cnt =0;
						m_UI_MainState_enter(RECORD_VOICE);
						show_msg("   RECORDING   ","     ERROR     ",MENU_STATE,6);
					}
					break;
				}
				default:
				break;			 
			}
			break;
		}
		case PLAY_CALL:
		{		
			switch(e_G_sub_state)
			{
				case PLAY_CALL_HOME:
				{					
					if(input==ENTER)
					{						 
						GSM_CommandSend(E_CALL_START,DEFAULT);
						e_G_sub_state = CALL_WAIT;
						lcd_update_hdlr(0,0,"WAITING...",16);
						lcd_update_hdlr(0,1," ",16);
						break;
					}
					else if(input==BACK)
					{
						m_UI_MainState_enter(PLAY_CALL);
						m_UI_PreCond_hdlr(e_G_main_state);
					}
					break;	
				}
				case CALL_WAIT:
				{
					if(input==BACK)
					{						 
						GSM_CommandSend(E_CALL_STOP,DEFAULT);
						e_G_sub_state = CMDRESP_WAIT;
						u8UI_timer_cnt =0;
					}
					else
					{
						u8UI_timer_cnt++;
						if(u8UI_timer_cnt > MAX_CALL_TIME)
						{
							GSM_CommandSend(E_CALL_STOP,DEFAULT);
							e_G_sub_state = CMDRESP_WAIT;
							u8UI_timer_cnt =0;
						}
					}					
					break;
				}
				case CMDRESP_WAIT:
				{
					u8UI_timer_cnt++;
					if(u8UI_timer_cnt > MAX_RESP_TIME)
					{
						u8UI_timer_cnt =0;
						m_UI_MainState_enter(PLAY_CALL);
						show_msg("   CALLING   ","     ERROR     ",MENU_STATE,6);
					}
					break;	
				}
				default:
				break;
				
			}			 
			break;
		}
		case TAKE_LOG:
		{	
			switch(e_G_sub_state)
			{
				case LOG_HOME:
				{			 
					if(input != 0)
					{							
						if(input == UP) 
						{
							s8LCD_disp_index=0;
							m_UI_PreCond_hdlr(e_G_main_state);
						}
						else if(input == DOWN)
						{
							s8LCD_disp_index=1;
							m_UI_PreCond_hdlr(e_G_main_state);
						}
						else if(input == ENTER)
						{
							e_G_sub_state = LOG_START_STATE + s8LCD_disp_index;
							s8LCD_disp_index = 0;							 
							u8LCD_nxt_disp_index = 1;
							m_UI_PreCond_hdlr(e_G_main_state);
						}	
						else if(input == BACK)
						{				
							m_UI_MainState_enter(TAKE_LOG);
							m_UI_PreCond_hdlr(e_G_main_state);				
						}	
					}
					break;
				}				
				case LOG_START_STATE:
				{					 
					if(input != 0)
					{
						if(input == ENTER)
						{						 
							e_G_sub_state = LOG_IN_PRO_STATE;
							s8LCD_disp_index = 0;
							m_UI_PreCond_hdlr(e_G_main_state); 
						}	
						else if(input == BACK)
						{							 
							e_G_sub_state = LOG_HOME;
							s8LCD_disp_index = 0;
							m_UI_PreCond_hdlr(e_G_main_state);							  
						}
					}
					break;
				}
				case LOG_DELETE_STATE:
				{					 
					if(input != 0)
					{
						if(input == ENTER)
						{					 
							e_G_sub_state = LOG_DEL_IN_PRO_STATE;
							s8LCD_disp_index = 0;
							m_UI_PreCond_hdlr(e_G_main_state); 
						}	
						else if(input == BACK)
						{							 
							e_G_sub_state = LOG_HOME;
							s8LCD_disp_index = 0;
							m_UI_PreCond_hdlr(e_G_main_state);
						}
					}
					break;
				}
				case LOG_IN_PRO_STATE:
				{	
					ret = E2P_LogRetrieve(LOG_START);
					if(ret == SUCCESS)
					{						 
						E2P_LogRetrieve(LOG_STP);
						e_G_sub_state = LOG_HOME;
						m_UI_PreCond_hdlr(e_G_main_state);
					}
					else
					{
						if(input == BACK)
						{
							E2P_LogRetrieve(LOG_STP);
							e_G_sub_state = LOG_HOME;
							m_UI_PreCond_hdlr(e_G_main_state);					 
						}
					}					
					break;
				}
				case LOG_DEL_IN_PRO_STATE:
				{
					E2P_LogReset();
					e_G_sub_state = LOG_HOME;
					m_UI_PreCond_hdlr(e_G_main_state);
					break;
				}
				default:
				break;
			}			 
			break;
		}
		case MODE_CTRL_CONFIG:
		{
			if(input != 0)
			{							
				if(input == UP) 
				{
					s8LCD_disp_index--;
					if(s8LCD_disp_index < 0)
					{
						s8LCD_disp_index = MODE_SET_LEN-1;
					}
					s8LCD_disp_index = s8LCD_disp_index % MODE_SET_LEN;
					u8LCD_nxt_disp_index = (s8LCD_disp_index + 1)% MODE_SET_LEN;
					m_UI_PreCond_hdlr(e_G_main_state);
				}
				else if(input == DOWN)
				{
					s8LCD_disp_index++;
					s8LCD_disp_index = s8LCD_disp_index % MODE_SET_LEN;
					u8LCD_nxt_disp_index = (s8LCD_disp_index + 1)% MODE_SET_LEN;
					m_UI_PreCond_hdlr(e_G_main_state);
				}
				else if(input == ENTER)
				{
					sys_data.u8_G_system_mode = s8LCD_disp_index;
					m_UI_MainState_enter(MODE_CTRL_CONFIG);
					m_UI_PreCond_hdlr(e_G_main_state);
					//e_G_main_state = MENU_STATE;
					//s8LCD_disp_index = 0;							 
					//u8LCD_nxt_disp_index = 1;
					//lcd_clear_screen();
					//m_UI_PreCond_hdlr(e_G_main_state);
				}	
				else if(input == BACK)
				{								 
					m_UI_MainState_enter(MODE_CTRL_CONFIG);
					m_UI_PreCond_hdlr(e_G_main_state);
				}	
			}
			break;		
		}
		case SELF_DIA:
		{
			switch(e_G_sub_state)
			{
				case SELF_DIA_HOME:
				{
					/*lcd_goToXy(1,1);				            
					lcd_printf("PRESS ENTER TO ");
					lcd_goToXy(1,2);
					lcd_printf(" START SELF DIA");*/
					if(input == ENTER)
					{
						//msg_to_send.command = SD_START;						 
						//ret = m_u8MSGQ_msg_send(&msg_to_send,BP_QUEUE);
						//if(ret == MSGQ_FULL)
						//{
							/********need to be handled**/
						//}
						//m_u8LED_ctl(T_START);
						e_G_sub_state = SELF_DIA_HOME;
						lcd_clear_screen();
						m_UI_PreCond_hdlr(e_G_main_state);
					}
					else if(input == BACK)
					{
						/*e_G_main_state = MENU_STATE;
						s8LCD_disp_index = SELF_DIA - MENU_INDEX_OFFSET;
						u8LCD_nxt_disp_index = (s8LCD_disp_index + 1)% DISP_MENU_LEN;
						lcd_clear_screen();*/
						
						m_UI_MainState_enter(SELF_DIA);
						m_UI_PreCond_hdlr(e_G_main_state);
					}					
					break;
				}
				case SELF_DIA_WAIT:
				{					 
					/*lcd_goToXy(1,1);					 
					lcd_printf("SELF DIA IN PRO");*/
					if(input == BACK)
					{
						//m_u8MSGQ_resp_send(TEST_COMPLETE,SUCCESS,BP_QUEUE);
						m_UI_PreCond_hdlr(e_G_main_state);
					}
					
					break;
				}
				default:
				break;
			}
			break;	
		}
		case SHOW_MSG:
		{
			delay_count--;
			if((delay_count == 0)||(input == BACK))
			{	
				e_G_main_state = first_digit(NXT_STATE);
				e_G_sub_state = NXT_STATE;	
				lcd_clear_screen();
				m_UI_PreCond_hdlr(e_G_main_state);
				delay_count=0;		
			}		
			break;
		}
		case SAVE_SETTINGS:
		{
			if(input != 0)
			{
				if((input == LEFT)&&(s8LCD_disp_index == SEL_RIGHT)) 
				{
					lcd_goToXy(s8LCD_disp_index,2);
					lcd_printf(" ");
					s8LCD_disp_index = SEL_LEFT;
					m_UI_PreCond_hdlr(e_G_main_state);
				}
				else if((input == RIGHT)&&(s8LCD_disp_index == SEL_LEFT))
				{
					lcd_goToXy(s8LCD_disp_index,2);
					lcd_printf(" ");
					s8LCD_disp_index = SEL_RIGHT;
					m_UI_PreCond_hdlr(e_G_main_state);
				}
				else if(input == ENTER)
				{					 
					if(s8LCD_disp_index == SEL_LEFT)
					{		
						m_u8MKEY_sts_set(sys_data.u8_G_system_mode);
						
						ret = E2P_CRI_STORE();
						if(ret == SUCCESS)
						{
							E2P_SendSYSdata();						
							e_G_main_state = UI_EXIT_STATE;
							u8UI_timer_cnt =0;
						}
						else
						{
							e_G_main_state = UI_EXIT_STATE;
							u8UI_timer_cnt =0;
						}					
					}
					else
					{
						ret = E2P_CRI_RETRIEVE();
						if(ret == SUCCESS)
						{
							G_ret = EXIT;
						}
						else
						{
							e_G_main_state = UI_EXIT_STATE;
							u8UI_timer_cnt =0;
						}
						 						
					}				 
					
				}			 	
			}
			break;
		}
		case CHANGE_PWD:
		{
			switch(e_G_sub_state)
			{
				case VALIDATE:
				{
					ret = u8_get_input_test(MAX_PWD_COUNT,&inp_ptr,input);//u8_get_input("ENTER PASSWORD ",MAX_PWD_COUNT,&inp_ptr,input);	
					if(ret!= WAITING)
					{
						if(ret == SUCCESS)
						{
							if(atoi(inp_ptr) == sys_data.u16_G_pss_wrd)
							{
								e_G_sub_state = GET_NEW_PWD;
								m_UI_PreCond_hdlr(e_G_main_state);
							}
							else
							{
							show_msg("INVALID       ","PASSWORD       ",VALIDATE,6);
							}
							clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);							 
						}
						else if(ret == EXIT)
						{
							e_G_main_state = MENU_STATE;
							s8LCD_disp_index = CHANGE_PWD - MENU_INDEX_OFFSET;
							u8LCD_nxt_disp_index = (s8LCD_disp_index + 1)% DISP_MENU_LEN;
							m_UI_PreCond_hdlr(e_G_main_state);
						}
					}					
					break;
				}
				case GET_NEW_PWD:
				{				
					ret = u8_get_input_test(MAX_PWD_COUNT,&inp_ptr,input);//u8_get_input("ENTER NEW PWD  ",MAX_PWD_COUNT,&inp_ptr,input);	
					if(ret != WAITING)
					{
						if(ret == SUCCESS)
						{						
							num = atoi(inp_ptr);
							if(num == 0)
							{
								show_msg("  PWD CANT BE  ","     ZERO      ",GET_NEW_PWD,6);	
							}
							else
							{
								sys_data.u16_G_pss_wrd = num;
								clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);							 
								s8LCD_disp_index = CHANGE_PWD - MENU_INDEX_OFFSET;
								u8LCD_nxt_disp_index = (s8LCD_disp_index + 1)% DISP_MENU_LEN;
								show_msg("PASSWORD SET   ","SUCCESS        ",MENU_STATE,6);
							}
							//EWrite_DataBuffer[PWD_START_INDEX]=(uint8_t)((num & 0xff00)>>8);
							//EWrite_DataBuffer[PWD_START_INDEX+1]=(uint8_t)(num & 0x00ff);
							
													 	
						}
						else if(ret == EXIT)
						{
							e_G_main_state = MENU_STATE;
							s8LCD_disp_index = CHANGE_PWD - MENU_INDEX_OFFSET;
							u8LCD_nxt_disp_index = (s8LCD_disp_index + 1)% DISP_MENU_LEN;
							m_UI_PreCond_hdlr(e_G_main_state);
						}
					}					
					break;
				}			
				default:
				break;
			}
		}
		case RESP_WAIT:
		{			 
			break;
		}
		case UI_EXIT_STATE:
		{
			/*G_ret = EXIT;				  
			e_G_main_state = MENU_STATE;	
			s16_UI_IdleTimeout = UI_IDLE_TIME_MAX;
			s8LCD_disp_index  =0;	
			u8LCD_nxt_disp_index = 1;*/
			u8UI_timer_cnt++;
			if(u8UI_timer_cnt > MAX_RESP_TIME)
			{
				u8UI_timer_cnt =0;
				G_ret = EXIT;
			}
			break;
		}
		default:
		break;
	} 
	return G_ret; 
}
void clear_inp(char *buff,uint16_t buff_len)
{
	uint16_t i =0;
	for(i = 0;i<buff_len;i++)
	{
		*buff = '\0';
		buff++;
	}
} 
void v_UI_varRESET(void)
{
	u8_G_usr_inp_ind = 0;
	clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);	
}
 uint8_t u8_get_input_test(uint8_t inp_len, char **usr_inp,char input)
{
	G_RESP_CODE ret = WAITING;	 
	if(inp_len >= MAX_LCD_TXT_LEN)
	{
	//v_cpy_str(1,1,"LEN_ERR");
	}
	else
	{
		/*if(u8_G_usr_inp_ind == 0)
		{
			lcd_goToXy(1,2);
			lcd_cursor_on();
			lcd_blink_on();
		}*/		
		if(input != 0)
		{
			if((input != ENTER) &&(input != BACK) && (u8_G_usr_inp_ind < (inp_len-1 )))
			{
				s8_G_usr_inp[u8_G_usr_inp_ind]=input;
				u8_G_usr_inp_ind++;
			}
			else if((input == BACK) && (u8_G_usr_inp_ind > 0))
			{
				u8_G_usr_inp_ind--;
				s8_G_usr_inp[u8_G_usr_inp_ind]=0;
			 
			}
			else if((input == BACK) && (u8_G_usr_inp_ind == 0))
			{
				lcd_clear_screen();
				clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);
				u8_G_usr_inp_ind = 0;
				//lcd_cursor_off();
				//lcd_blink_off();
				ret = EXIT;
			}
			else if(input == ENTER)
			{
				lcd_clear_screen();
				s8_G_usr_inp[u8_G_usr_inp_ind] = '\0';		 
				u8_G_usr_inp_ind = 0;
				*usr_inp = s8_G_usr_inp;
				//lcd_cursor_off();
				//lcd_blink_off();
				ret = SUCCESS;
			}			 
			lcd_update_hdlr(0,1,s8_G_usr_inp,inp_len);
			if(ret != EXIT)
			{
				lcd_goToXy(u8_G_usr_inp_ind+1,2);
				//lcd_cursor_on();
				//lcd_blink_on();
			}			
		}
	}
	return ret;
}

 uint8_t u8_get_zone_dly(uint8_t inp_len, char **usr_inp,char input)
{
	G_RESP_CODE ret = WAITING;	 
	if(inp_len >= MAX_LCD_TXT_LEN)
	{
	//v_cpy_str(1,1,"LEN_ERR");
	}
	else
	{
		/*if(u8_G_usr_inp_ind == 0)
		{
			lcd_goToXy(1,2);
			lcd_cursor_on();
			lcd_blink_on();
		}*/		
		if(input != 0)
		{
			if((input != ENTER) &&(input != BACK) && (u8_G_usr_inp_ind < (inp_len-1 )))
			{
				s8_G_usr_inp[u8_G_usr_inp_ind]=input;
				u8_G_usr_inp_ind++;
			}
			else if((input == BACK) && (u8_G_usr_inp_ind > 0))
			{
				u8_G_usr_inp_ind--;
				s8_G_usr_inp[u8_G_usr_inp_ind]=0;
			 
			}
			else if((input == BACK) && (u8_G_usr_inp_ind == 0))
			{
				lcd_clear_screen();
				clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);
				u8_G_usr_inp_ind = 0;
				//lcd_cursor_off();
				//lcd_blink_off();
				ret = EXIT;
			}
			else if(input == ENTER)
			{
				lcd_clear_screen();
				s8_G_usr_inp[u8_G_usr_inp_ind] = '\0';		 
				u8_G_usr_inp_ind = 0;
				*usr_inp = s8_G_usr_inp;
				//lcd_cursor_off();
				//lcd_blink_off();
				ret = SUCCESS;
			}			 
			lcd_update_hdlr(7,1,s8_G_usr_inp,inp_len);
			if(ret != EXIT)
			{
				lcd_goToXy(u8_G_usr_inp_ind+1,2);
				//lcd_cursor_on();
				//lcd_blink_on();
			}			
		}
	}
	return ret;
}

void clear_pwd_inp(void)
{
	clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);
	u8_G_usr_inp_ind = 0;
}

uint8_t u8_get_input(char* disp_msg,uint8_t inp_len, char **usr_inp,char input)
{
	G_RESP_CODE ret = WAITING;	 
	if(inp_len >= MAX_LCD_TXT_LEN)
	{
	//v_cpy_str(1,1,"LEN_ERR");
	}
	else
	{
	//fill_space(s8_G_usr_inp,MAX_LCD_TXT_LEN);
	if(NULL!=disp_msg)
	{
		lcd_goToXy(1,1);
		lcd_printf(disp_msg);			
	}	
	lcd_goToXy(1,2);
	//lcd_PrintSP(s8_G_usr_inp);
	lcd_PrintSP_d(s8_G_usr_inp,inp_len);
	if(input != 0)
	{
		if((input != ENTER) &&(input != BACK) && (u8_G_usr_inp_ind < (inp_len-1 )))
		{
			s8_G_usr_inp[u8_G_usr_inp_ind]=input;
			u8_G_usr_inp_ind++;
		}
		else if((input == BACK) && (u8_G_usr_inp_ind > 0))
		{
			u8_G_usr_inp_ind--;
			s8_G_usr_inp[u8_G_usr_inp_ind]=0;
		 
		}
		else if((input == BACK) && (u8_G_usr_inp_ind == 0))
		{
			lcd_clear_screen();
			clear_inp(s8_G_usr_inp,MAX_LCD_TXT_LEN);
			u8_G_usr_inp_ind = 0;
			ret = EXIT;
		}
		else if(input == ENTER)
		{
			lcd_clear_screen();
			s8_G_usr_inp[u8_G_usr_inp_ind] = '\0';		 
			u8_G_usr_inp_ind = 0;
			*usr_inp = s8_G_usr_inp;
			ret = SUCCESS;
		}		 
	}
	}
	return ret;
}

void show_msg(char *string1,char *string2,uint8_t next_state,uint8_t delay_time)
{   
  //LCD_ROW1_MSG = string1;
  //LCD_ROW2_MSG = string2;
  
  
  lcd_update_hdlr(0,0,string1,16);
  lcd_update_hdlr(0,1,string2,16);
  NXT_STATE = next_state;
  delay_count = delay_time;
  e_G_main_state = SHOW_MSG;
  //lcd_clear_screen();  
}

uint8_t first_digit(uint8_t num)
{
	char stri[4];
	uint8_t digit;
	/*itoa(num, stri, 10);
	stri[1]=stri[2]=stri[3]='\0';
	digit = (uint8_t)atoi(stri);*/
	if(num >= 10)
	{
		digit = num/10;
	}
	else
	{
		digit = num;
	}
	return digit;
}

void v_arr_store_char(uint8_t index,char *buff)
{
	uint16_t i=0;
	if(index < 10)
	{
		for(i=0;i<PHONE_NO_OFFSET;i++)
		{
		ph_no[index].number[i]=*(buff);
		buff++;
		}
	}
}


/*void v_arr_store_char(uint16_t index,uint16_t offset,char *buff)
{
	uint16_t i=0;
	for(i=index;i<(index+offset);i++)
	{
	EWrite_DataBuffer[i]=*(buff);
	buff++;
	}
	
}*/
/*
uint8_t u8_arr_store_ascii(uint16_t index,uint16_t offset,uint16_t num)
{
	uint16_t i=0;
	u8E2P_buff[index]=(uint8_t)(num & 0xff00)>>8;
	u8E2P_buff[index+1]=(uint8_t)(num & 0x00ff);
	
	return SUCCESS;
}   */   




void UI_sysDATtoArray(uint8_t *arr)
{
	uint8_t i=0;
	uint8_t l =0;
	uint8_t zone = 0;
	uint8_t *buff_ptr = NULL;
	
	buff_ptr = arr + PHONE_NO_START_INDEX;
	
	
	for(i=0;i<PH_NO_LIST_LEN;i++)
	{
		for(l=0;l<PHONE_NO_OFFSET;l++)
		{
			*(buff_ptr++)=ph_no[i].number[l];			 
		}
	}
	buff_ptr = arr + PH_NO_STATUS_START_INDEX;
	for(i=0;i<PH_NO_LIST_LEN;i++)
	{
		*(buff_ptr++) = ph_no[i].no_state;		 
	}
	
	
	buff_ptr = arr + ZONE_SETTINGS_START_INDEX;
	for(zone = 0;zone < ZONE_COUNT;zone++)
	{
		*(buff_ptr++) = sensor_index[zone].active_sts;
		 
		*(buff_ptr++) = sensor_index[zone].mode_setting ;
		 
		*(buff_ptr++) = sensor_index[zone].trig_setting;		
		 
		*(buff_ptr++) = (uint8_t)((sensor_index[zone].delay_timing & 0xff00)>>8);
		 
		*(buff_ptr++) = (uint8_t)(sensor_index[zone].delay_timing & 0x00ff);		 
	}
	
	buff_ptr = arr + PWD_START_INDEX;
	*(buff_ptr++) = (uint8_t)((sys_data.u16_G_pss_wrd & 0xff00)>>8);
	*(buff_ptr++) = (uint8_t)(sys_data.u16_G_pss_wrd & 0x00ff);
	
	buff_ptr = arr + SIREN_STATE_START_INDEX;	
	*(buff_ptr++) = sys_data.u8_G_siren_state;
	
	buff_ptr = arr + SIREN_TIME_START_INDEX;	
	*(buff_ptr++) = (uint8_t)((sys_data.u16_G_siren_time & 0xff00)>>8);
	*(buff_ptr++) = (uint8_t)(sys_data.u16_G_siren_time & 0x00ff);
	
	buff_ptr = arr + MODE_CTRL_CONFIG_START_INDEX;
	*(buff_ptr++) = sys_data.u8_G_system_mode_control;
	
	buff_ptr = arr + CURRENT_SYS_MODE_START_INDEX;
	*(buff_ptr++) = sys_data.u8_G_system_mode;
}

void UI_sysDATtoStruct(uint8_t *arr)
{
	uint8_t *buff_ptr = 0;
	uint8_t l=0;
	uint8_t i=0;
	buff_ptr = arr + PHONE_NO_START_INDEX;
	
	for(i=0;i<PH_NO_LIST_LEN;i++)
	{
		for(l=0;l<PHONE_NO_OFFSET;l++)
		{
			ph_no[i].number[l] = *(buff_ptr++);
		}
	}
	buff_ptr = arr + PH_NO_STATUS_START_INDEX;
	for(i=0;i<PH_NO_LIST_LEN;i++)
	{
		ph_no[i].no_state =*(buff_ptr++);
	}
	
	buff_ptr = arr + PWD_START_INDEX;
	
	#ifdef PWD_DEFAULT
	sys_data.u16_G_pss_wrd = 1234;
	#else
	sys_data.u16_G_pss_wrd = (uint16_t)*(buff_ptr++);
	sys_data.u16_G_pss_wrd = (sys_data.u16_G_pss_wrd << 8);
	sys_data.u16_G_pss_wrd = sys_data.u16_G_pss_wrd | (uint16_t)*(buff_ptr++);
	#endif

	buff_ptr = arr + SIREN_STATE_START_INDEX;
	sys_data.u8_G_siren_state =*(buff_ptr++);
	
	buff_ptr = arr + SIREN_TIME_START_INDEX;
	sys_data.u16_G_siren_time = (uint16_t)*(buff_ptr++);
	sys_data.u16_G_siren_time = (sys_data.u16_G_siren_time << 8);
	sys_data.u16_G_siren_time = (sys_data.u16_G_siren_time)| (uint16_t)*(buff_ptr++);
	
	//sys_data.u16_G_siren_time = (uint16_t)mulu((uint8_t)sys_data.u16_G_siren_time,120);
	
	
	sys_data.u8_G_system_mode_control = *(arr + MODE_CTRL_CONFIG_START_INDEX); 
	sys_data.u8_G_system_mode =*(arr + CURRENT_SYS_MODE_START_INDEX);
	m_u8ModeHdlr_update(arr);	
	
	buff_ptr = arr + ZONE_SETTINGS_START_INDEX;	
	for(i = 0;i < ZONE_COUNT;i++)
	{
		sensor_index[i].active_sts = *(buff_ptr++);
		sensor_index[i].mode_setting =  *(buff_ptr++);
		sensor_index[i].trig_setting =  *(buff_ptr++);
		sensor_index[i].delay_timing = (uint16_t) *(buff_ptr++);
		sensor_index[i].delay_timing = sensor_index[i].delay_timing << 8;
		sensor_index[i].delay_timing = (sensor_index[i].delay_timing)|(uint16_t) *(buff_ptr++);
	
		//sensor_index[zone].delay_timing = (uint16_t)mulu((uint8_t)sensor_index[zone].delay_timing,2);
	}	
}
/*
G_RESP_CODE m_u8fill_ph_details(uint8_t *arr)
{
	uint8_t *buff_ptr = 0;
	uint8_t l=0;
	uint8_t i=0;
	buff_ptr = arr + PHONE_NO_START_INDEX;
	
	for(i=0;i<PH_NO_LIST_LEN;i++)
	{
		for(l=0;l<PHONE_NO_OFFSET;l++)
		{
			ph_no[i].number[l] = *(buff_ptr++);
		}
	}
	buff_ptr = arr + PH_NO_STATUS_START_INDEX;
	for(i=0;i<PH_NO_LIST_LEN;i++)
	{
		ph_no[i].no_state =*(buff_ptr++);
	}
	return 0;
}
G_RESP_CODE m_u8fill_zone_details(uint8_t *arr)
{
	uint8_t *buff_ptr = 0;
	uint8_t zone=0;	 
	buff_ptr = arr + ZONE_SETTINGS_START_INDEX;
	
	for(zone = 0;zone < ZONE_COUNT;zone++)
	{
		sensor_index[zone].active_sts = *(buff_ptr++);
		sensor_index[zone].mode_setting =  *(buff_ptr++);
		sensor_index[zone].trig_setting =  *(buff_ptr++);
		sensor_index[zone].delay_timing = (uint16_t) *(buff_ptr++);
		sensor_index[zone].delay_timing = sensor_index[zone].delay_timing << 8;
		sensor_index[zone].delay_timing = (sensor_index[zone].delay_timing)|(uint16_t) *(buff_ptr++);
	
		//sensor_index[zone].delay_timing = (uint16_t)mulu((uint8_t)sensor_index[zone].delay_timing,2);
	}
	
	return 0;
}
G_RESP_CODE m_u8fill_sys_details(uint8_t *arr)
{
	uint8_t *buff_ptr = 0;	 
	buff_ptr = arr + PWD_START_INDEX;
	
	#ifdef PWD_DEFAULT
	sys_data.u16_G_pss_wrd = 1234;
	#else
	sys_data.u16_G_pss_wrd = (uint16_t)*(buff_ptr++);
	sys_data.u16_G_pss_wrd = (sys_data.u16_G_pss_wrd << 8);
	sys_data.u16_G_pss_wrd = sys_data.u16_G_pss_wrd | (uint16_t)*(buff_ptr++);
	#endif

	buff_ptr = arr + SIREN_STATE_START_INDEX;
	sys_data.u8_G_siren_state =*(buff_ptr++);
	
	buff_ptr = arr + SIREN_TIME_START_INDEX;
	sys_data.u16_G_siren_time = (uint16_t)*(buff_ptr++);
	sys_data.u16_G_siren_time = (sys_data.u16_G_siren_time << 8);
	sys_data.u16_G_siren_time = (sys_data.u16_G_siren_time)| (uint16_t)*(buff_ptr++);
	
	//sys_data.u16_G_siren_time = (uint16_t)mulu((uint8_t)sys_data.u16_G_siren_time,120);
	
	
	sys_data.u8_G_system_mode_control = *(arr + MODE_CTRL_CONFIG_START_INDEX); 
	sys_data.u8_G_system_mode =*(arr + CURRENT_SYS_MODE_START_INDEX);
	m_u8ModeHdlr_update(EWrite_DataBuffer);	
	return 0;
}
*/
void factory_reset_process()
{	 
	uint8_t l=0;
	uint8_t i=0;
	 
	for(i=0;i<PH_NO_LIST_LEN;i++)
	{
		for(l=0;l<PHONE_NO_OFFSET;l++)
		{
			ph_no[i].number[l] = '\0';
		}
	}	 
	for(i=0;i<PH_NO_LIST_LEN;i++)
	{
		ph_no[i].no_state = ISOLATE;
	}	 
	sys_data.u16_G_pss_wrd = 1234; 
	sys_data.u8_G_siren_state =ON;
	sys_data.u16_G_siren_time = 3;	
	
	sys_data.u8_G_system_mode_control = EXT_KEY; 
	sys_data.u8_G_system_mode = DEFAULT_MODE;
	 
	
	sensor_index[0].active_sts = ACTIVATE;
	sensor_index[0].trig_setting =  DEFAULT_TRIG;
	sensor_index[0].mode_setting =  ARM_MODE;
	sensor_index[0].delay_timing = 20;
	
	sensor_index[1].active_sts = ACTIVATE;
	sensor_index[1].trig_setting =  DEFAULT_TRIG;
	sensor_index[1].mode_setting =  ARM_MODE;
	sensor_index[1].delay_timing = 0;
	
	sensor_index[2].active_sts = ACTIVATE;
	sensor_index[2].trig_setting =  DEFAULT_TRIG;
	sensor_index[2].mode_setting =  PART_ARM_MODE;
	sensor_index[2].delay_timing = 0;
	
	sensor_index[3].active_sts = ACTIVATE;
	sensor_index[3].trig_setting =  DEFAULT_TRIG;
	sensor_index[3].mode_setting =  DISARM_MODE;
	sensor_index[3].delay_timing = 0;
	
	m_u8ZONE_reset();
	m_u8MKEY_sts_set(sys_data.u8_G_system_mode);
	E2P_SendSYSdata();
}

uint16_t m_u16_get_pwd()
{
	return sys_data.u16_G_pss_wrd;
} 
void m_UI_MainState_enter(MAIN_STATE_T curr_state) 
{
	e_G_sub_state = curr_state * 10;
	e_G_main_state = MENU_STATE;
	s8LCD_disp_index = curr_state-MENU_INDEX_OFFSET;
	u8LCD_nxt_disp_index = (s8LCD_disp_index + 1)% DISP_MENU_LEN;
	lcd_clear_screen();
}
void m_UI_PreCond_hdlr(MAIN_STATE_T curr_state)
{
	switch(curr_state)
	{
		case MENU_STATE:
		{
			lcd_update_hdlr(0,0,">",1);
			lcd_update_hdlr(0,1," ",1);
			lcd_update_hdlr(1,0,s8_G_display_menu[s8LCD_disp_index],16);
			lcd_update_hdlr(1,1,s8_G_display_menu[u8LCD_nxt_disp_index],16);
			break;
		}		
		case PLAY_SETTINGS:
		{
			switch(e_G_sub_state)
			{
				case PLAY_SETTING_OPTIONS :
				{	
					lcd_update_hdlr(0,s8LCD_disp_index,">",1);
					lcd_update_hdlr(0,(!s8LCD_disp_index)," ",1);
					lcd_update_hdlr(1,0,"PLAY ZONES    ",16);
					lcd_update_hdlr(1,1,"PLAY PHONE NO ",16);
					break;
				}
				case PLAY_ZONE_SETTINGS :
				{	
					/*lcd_update_hdlr(0,0,"ZONE",4);
					lcd_printnum(4,0,s8LCD_disp_index+1,ZONE_NO_PRINT_LEN+3);//+3 to add 3 space
					lcd_printnum(9,0,sensor_index[s8LCD_disp_index].delay_timing,DEFAULT_PRINT_LEN+3);//+3 to add 3 space
					lcd_update_hdlr(0,1," ",3);
					lcd_update_hdlr(3,1,s8_G_active_isolate[sensor_index[s8LCD_disp_index].active_sts],2+2);//+2 to add two space
					lcd_update_hdlr(7,1,s8_G_NO_NC[sensor_index[s8LCD_disp_index].trig_setting],3+6);//+6 to add 6 space 
					*/
					lcd_update_hdlr(0,0," ",1);
					lcd_update_hdlr(1,0,"ZONE",4);
					lcd_printnum(5,0,s8LCD_disp_index+1,1);
					lcd_update_hdlr(6,0," ",1);
					lcd_update_hdlr(7,0,s8_G_act_iso[sensor_index[s8LCD_disp_index].active_sts],4);
					lcd_update_hdlr(11,0," ",1);
					lcd_update_hdlr(12,0,s8_G_NO_NC[sensor_index[s8LCD_disp_index].trig_setting],4);
					lcd_update_hdlr(0,1," ",2);
					lcd_update_hdlr(2,1,s8_G_arm_parm[sensor_index[s8LCD_disp_index].mode_setting],3);
					lcd_update_hdlr(5,1," ",1);
					lcd_update_hdlr(6,1,"D",1);
					lcd_printnum(7,1,sensor_index[s8LCD_disp_index].delay_timing,4);
					lcd_update_hdlr(10,1,"secs",7);
					break;
				}
				case PLAY_PHONE_NO :
				{					 
					lcd_update_hdlr(0,0,s8_G_phone_book[s8LCD_disp_index],PH_BOOK_LEN+6);//+6 to add 6 space							 
					lcd_update_hdlr(13,0,s8_G_active_isolate[ph_no[s8LCD_disp_index].no_state],1+2);//+2 to add 2 space
					lcd_update_hdlr(0,1," ",2);
					lcd_update_hdlr(2,1,ph_no[s8LCD_disp_index].number,PHONE_NO_OFFSET);
					break;
				}
				default:
				break;
			}
			break;
		}
		case PH_NO_CONFIG:
		{
			switch(e_G_sub_state)
			{
				case PHONE_NO_CONFIG_HOME :
				{	
					
					lcd_update_hdlr(0,0,">",1);							 
					lcd_update_hdlr(1,0,s8_G_phone_book[s8LCD_disp_index],PH_BOOK_LEN+7);
					lcd_update_hdlr(14,0,s8_G_active_isolate[ph_no[s8LCD_disp_index].no_state],2);
					lcd_update_hdlr(0,1,ph_no[s8LCD_disp_index].number,PHONE_NO_OFFSET);
					break;
				}
				case GET_NUMBERS :
				{					 
					lcd_update_hdlr(0,0,"ENTER PH NO",16);
					//lcd_update_hdlr(8,1,"in secs",8);
					//lcd_update_hdlr(0,0,"ENTER DELAY ",16);
					lcd_update_hdlr(0,1," ",16);
					break;
				}
				case SET_NUMBERS :
				{	
					lcd_update_hdlr(0,s8LCD_disp_index,">",1);							 
					lcd_update_hdlr(0,(!s8LCD_disp_index)," ",1);
					lcd_update_hdlr(1,0,"ISOLATE",16);
					lcd_update_hdlr(1,1,"ACTIVATE",16);
					break;
				}
				default:
				break;
			}
			break;
		}
		case ZONE_SETTING_CONF:
		{
			lcd_update_hdlr(0,0," ",1);
			lcd_update_hdlr(1,0,"ZONE",4);
			lcd_printnum(5,0,s8LCD_disp_index+1,1);
			lcd_update_hdlr(6,0," ",1);
			lcd_update_hdlr(7,0,s8_G_act_iso[sensor_index[s8LCD_disp_index].active_sts],4);
			lcd_update_hdlr(11,0," ",1);
			lcd_update_hdlr(12,0,s8_G_NO_NC[sensor_index[s8LCD_disp_index].trig_setting],4);
			lcd_update_hdlr(0,1," ",2);
			lcd_update_hdlr(2,1,s8_G_arm_parm[sensor_index[s8LCD_disp_index].mode_setting],3);
			lcd_update_hdlr(5,1," ",1);
			lcd_update_hdlr(6,1,"D",1);
			lcd_printnum(7,1,sensor_index[s8LCD_disp_index].delay_timing,3);
			lcd_update_hdlr(10,1,"secs",6);
			
			switch(e_G_sub_state)
			{
				case ZONE_SELECT:
				{
					lcd_update_hdlr(0,0,">",1);
					break;
				}
				case ZONE_CONF:
				{
					lcd_update_hdlr(6,0,">",1);
					break;
				}
				case ZONE_TRIG_CONF:
				{
					lcd_update_hdlr(11,0,">",1);
					
					break;
				}
				case ZONE_STATE_CONF:
				{
					lcd_update_hdlr(1,1,">",1);
					break;
				}
				case ZONE_ENTER_CHECK:
				{
					lcd_update_hdlr(5,1,">",1);
					
					break;
				}
				case ZONE_E_DELAY_CONF:
				{
					lcd_update_hdlr(5,1,">",1);
					lcd_update_hdlr(7,1," ",3);
					break;
				}
				default:
				break;
			}			 
			break;
		}
		case SIREN_SETTINGS:
		{
			switch(e_G_sub_state)
			{
				case SIREN_OPTIONS:
				{				
					lcd_update_hdlr(0,s8LCD_disp_index,">",1);							 
					lcd_update_hdlr(0,(!s8LCD_disp_index)," ",1);
					lcd_update_hdlr(1,0,"SIREN STATE",16);
					lcd_update_hdlr(1,1,"SIREN TIME ",16);
					break;
				}
				case SIREN_STATE:
				{
					lcd_update_hdlr(0,s8LCD_disp_index,">",1);							 
					lcd_update_hdlr(0,(!s8LCD_disp_index)," ",1);
					lcd_update_hdlr(1,0,"SIREN OFF",16);
					lcd_update_hdlr(1,1,"SIREN ON",16);
					lcd_update_hdlr(13,sys_data.u8_G_siren_state,"_",3);
					break;
				}
				case SIREN_TIME:
				{				
					lcd_update_hdlr(0,0,"ENTER TIME",16);
					lcd_printnum(12,0,sys_data.u16_G_siren_time,MAX_DELAY_COUNT);
					lcd_update_hdlr(0,1," ",8);
					lcd_update_hdlr(8,1,"in mins",8);
					break;
				}
				default:
				break;
			}
			break;
		}
		case RECORD_VOICE:
		{
			if(u8get_GSM_sts() == GSM_OK)
			{
				lcd_update_hdlr(0,0," HOLD 4 TO ",16);							 
				lcd_update_hdlr(0,1,"START RECORDING",16);
				break;
			}
			else
			{
				m_UI_MainState_enter(RECORD_VOICE);					     
				show_msg(" GSM NOT READY","               ",MENU_STATE,6);				 
			}
			break;
		}
		case PLAY_CALL:
		{
			if(u8get_GSM_sts() == GSM_OK)
			{
				if(ACTIVATE == ph_no[0].no_state)
				{
					lcd_update_hdlr(0,0," PRESS ENTER   ",16);							 
					lcd_update_hdlr(0,1,"    TO DIAL    ",16);
				}
				else
				{
					m_UI_MainState_enter(PLAY_CALL);					     
					show_msg("   SET ADMIN  ","      FIRST    ",MENU_STATE,6);
				}
				
				break;
			}
			else
			{
				m_UI_MainState_enter(PLAY_CALL);					     
				show_msg(" GSM NOT READY","               ",MENU_STATE,6);
			}			 
			break;
		}
		case CHANGE_PWD:
		{
			switch(e_G_sub_state)
			{
				case VALIDATE   :
				{	
					lcd_update_hdlr(0,0," ENTER PASSWORD ",16);
					lcd_update_hdlr(0,1,"  ",16);
					break;
				}
				case GET_NEW_PWD :
				{	
					lcd_update_hdlr(0,0," ENTER NEW PWD ",16);
					lcd_update_hdlr(0,1,"  ",16);
					break;
				}
				default:
				break;
			}
			break;
		}
		case TAKE_LOG:
		{	
			switch(e_G_sub_state)
			{
				case LOG_HOME:
				{				
					lcd_update_hdlr(0,s8LCD_disp_index,">",1);							 
					lcd_update_hdlr(0,(!s8LCD_disp_index)," ",1);
					lcd_update_hdlr(1,0,"  START LOG    ",15);
					lcd_update_hdlr(1,1,"   DELETE LOG  ",15);
					break;
				}				
				case LOG_START_STATE:
				{					 
					lcd_update_hdlr(0,0,"PRESS ENTER    ",16);
					lcd_update_hdlr(0,1," TO TAKE LOG   ",16);
					break;
				}
				case LOG_DELETE_STATE:
				{					 
					lcd_update_hdlr(0,0,"PRESS ENTER    ",16);
					lcd_update_hdlr(0,1," TO DELETE LOG ",16);
					break;
				}
				case LOG_IN_PRO_STATE:
				{					 
					lcd_update_hdlr(0,0,"    LOGGING    ",16);
					lcd_update_hdlr(0,1,"  PLEASE WAIT  ",16);
					break;
				}
				case LOG_DEL_IN_PRO_STATE:
				{
					lcd_update_hdlr(0,0," LOG DELETING ",16);
					lcd_update_hdlr(0,1,"  PLEASE WAIT  ",16);
					break;
				}
				default:
				break;
			}			 
			break;
		}
		case MODE_CTRL_CONFIG:
		{
			lcd_update_hdlr(0,0,">",1);
			lcd_update_hdlr(0,1," ",1);
			lcd_update_hdlr(1,0,s8_G_mode_setting_options[s8LCD_disp_index],16);
			lcd_update_hdlr(1,1,s8_G_mode_setting_options[u8LCD_nxt_disp_index],16);
			break;		
		}
		case SELF_DIA:
		{
			switch(e_G_sub_state)
			{
				case SELF_DIA_HOME :
				{	
					lcd_update_hdlr(0,0,"NOT READY      ",16);
					lcd_update_hdlr(0,1,"      ",16);
					//lcd_update_hdlr(0,0,"PRESS ENTER TO",16);
					//lcd_update_hdlr(0,1,"START SELF DIA",16);
					break;
				}
				case SELF_DIA_WAIT :
				{	
					lcd_update_hdlr(0,0,"SELF DIA IN PRO",16);
					break;
				}
				default:
				break;
			}
			break;
		}
		case SAVE_SETTINGS:
		{
			lcd_update_hdlr(0,1," ",16);
			lcd_update_hdlr(0,0,"SAVE SETTINGS",16);
			lcd_update_hdlr(2,1,"YES",6);
			lcd_update_hdlr(9,1,"NO",7);
			  
			lcd_update_hdlr(s8LCD_disp_index,1,">",1);
			
		}
		default:
		break;
	}
} 
void m_UI_Main_PreCond(void)
{
	s8LCD_disp_index = 0;
	u8LCD_nxt_disp_index = 1;
	e_G_main_state=MENU_STATE;
	s16_UI_IdleTimeout = UI_IDLE_TIME_MAX;
	v_UI_varRESET();
	lcd_update_hdlr(0,0,">",16);
	lcd_update_hdlr(0,1," ",16);
	lcd_update_hdlr(1,0,s8_G_display_menu[s8LCD_disp_index],16);
	lcd_update_hdlr(1,1,s8_G_display_menu[u8LCD_nxt_disp_index],16);
	sys_data.u8_G_system_mode = m_u8MKEY_sts_get();
}

uint16_t m_UI_get_siren_time(void)
{
uint16_t ret = sys_data.u16_G_siren_time;
return ret;
}

G_RESP_CODE UI_eventManager(E_SYS_EVENTS event,uint8_t sts)
{
	G_RESP_CODE ret = WAITING;
	switch(e_G_main_state)	
	{
		case RECORD_VOICE:
		{
			switch(event)
			{
				case E_REC_START:
				{
					if(sts ==  SUCCESS)
					{
						e_G_sub_state = RECORDING;
						u8UI_timer_cnt = 0;
						lcd_update_hdlr(0,0,"  RECORDING..",16);
						lcd_update_hdlr(0,1," ",16);
					}
					else if(sts ==  FAILURE)
					{
						show_msg("REC_START      ","ERROR          ",RECORD_VOICE_HOME,6);
					}
					break;
				}
				case E_REC_STOP:
				{
					if(sts ==  SUCCESS)
					{
						m_UI_MainState_enter(RECORD_VOICE);
						show_msg("  RECORDING    ","   STOPPED     ",MENU_STATE,6);
					}
					else if(sts ==  FAILURE)
					{
						show_msg("  RECORD STOP  ","    ERROR      ",RECORD_VOICE_HOME,6);
					}
					break;
				}
				case E_POWER:
				{
					if((sts ==  BAT_LOW) || (sts ==  BAT_SHUT))
					{
						m_UI_MainState_enter(RECORD_VOICE);
						show_msg("GSM TURNED     ","OFF            ",MENU_STATE,6);
					}					 
					break;
				}
				default:
				break;
			}	 		 
			break;
		}
		case PLAY_CALL:
		{
			switch(event)
			{
				case E_CALL_STS:
				{
					if(sts == CALL_ESTB)
					{
						lcd_update_hdlr(0,0,"CALL ESTB",16);
						lcd_update_hdlr(0,1," ",16);
					}
					else if(sts == CALL_ANS)
					{
						lcd_update_hdlr(0,0,"CALL ANSWERED",16);
						lcd_update_hdlr(0,1," ",16);
					}
					else if(sts == CALL_HANG)
					{
						m_UI_MainState_enter(PLAY_CALL);						 
						show_msg(" CALL  HANGED  ","               ",MENU_STATE,6);
					}						
					break;
				}
				case E_CALL_STOP:
				{
					m_UI_MainState_enter(PLAY_CALL);						 
					show_msg(" CALL  HANGED  ","               ",MENU_STATE,6);
					break;
				}
				case E_POWER:
				{
					if((sts ==  BAT_LOW) || (sts ==  BAT_SHUT))
					{
						m_UI_MainState_enter(RECORD_VOICE);
						show_msg("GSM TURNED     ","OFF            ",MENU_STATE,6);
					}					 
					break;
				}
				default:
				break;
			}			 
			break;
		}
		case UI_EXIT_STATE:
		{
			switch(event)
			{
				case E_E2P_SEND:
				{
					if(sts!=WAITING)
					{
						ret = EXIT;
					}
					break;
				}
				case E_E2P_CRI_STORE:
				{
					E2P_SendSYSdata();						
					e_G_main_state = UI_EXIT_STATE;
					u8UI_timer_cnt =0;
					break;
				}
				case E_E2P_CRI_READ:
				{
					ret = EXIT;
					break;
				}			
				default:
				break;
			}
			
			
			break;
		}	 
		default:
		break;
		
	}
	return ret;
}