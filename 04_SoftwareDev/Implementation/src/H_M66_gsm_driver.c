#include "H_M66_gsm_driver.h"
#include "M_time_out.h"
#include "M_gsm_handler.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "r_cg_userdefine.h"
#include "r_cg_serial.h"
#include "H_delay_driver.h"
#include "M_message_queue_handler.h"
#include "M_sys_config.h"
#include "M_GeneralLib.h" 

#define MAX_CIRC_BUFF_DEPTH 100
#define GSM_TIMEOUT_VAL 10 //100ms units

#define GSM_INIT_WAIT_INTERVAL 10 //500ms units
#define AT_RETRY_WAIT_INT 10// 

#define GSM_LDO_ENABLE P2.5


#define MAX_AT_TIMEOUT_CNT 15
#define MAX_AT_RETRY_CNT 3
#define MAX_CUSTOM_AT_SIZE 30



#define CTRLZ 26 // This val is related to M66 , dont change the value


const uint8_t AT[]={'A','T','\r','\n'};  // response OK (to check status)
const uint8_t ATCPIN[]={'A','T','+','C','P','I','N','?','\r'}; //response OK (to check SIM status, Inserted or Not,Locked or Not.)
const uint8_t ATCMGF[]={'A','T','+','C','M','G','F','=','1','\r'};  //response OK (SMS to set in Text mode.)
const uint8_t ATCNMI[]={'A','T','+','C','N','M','I','=','2',',','1',',','0',',','0',',','0','\r'}; // to STORE INCOMMING SMS.
const uint8_t ATCMGD[]={'A','T','+','C','M','G','D','=','1',',','4','\r'};  //response OK (To DELETE 1ST MEMORY SMS.) 
const uint8_t ATGSN[]={'A','T','+','G','S','N','\r'}; // to get SIM IMEI Number.
const uint8_t ATE0[]={'A','T','E','0','\r'}; // Echo Off
const uint8_t ATCLCC[]={'A','T','+','C','L','C','C','\r'}; // Call_status
const uint8_t ATQFDEL[]={'A','T','+','Q','F','D','E','L','=','"','f','i','r','e','.','a','m','r','"','\r','\0'}; // FileDelete
const uint8_t ATQFDELA[]={'A','T','+','Q','F','D','E','L','=','"','*','"','\r','\0'}; // FileDelete
const uint8_t ATH[]="ATH\r";
const uint8_t ATQAUDRD[]="AT+QAUDRD=1,\"fire.amr\",3\r"; // start record
const uint8_t ATQAUDRDS[]="AT+QAUDRD=0,\"fire.amr\",3\r"; // stop record
const uint8_t ATQPSND[]="AT+QPSND=1,\"fire.amr\",0,7,7,0\r";//START PLAYBACK
const uint8_t ATQPSNDS[]="AT+QPSND=0,\"fire.amr\",0,7,7,0\r";//STOP PLAYBACK
const uint8_t ATQFLDS[]="AT+QFLDS=\"UFS\"\r";
const uint8_t ATQMOSTAT[]="AT+QMOSTAT=1\r";
const uint8_t ATIPR[]="AT+IPR=9600\r";
const uint8_t ATCTZU[]="AT+CTZU=3\r";
const uint8_t ATCCLK[]="AT+CCLK?\r";
const uint8_t ATCSCS[] = "AT+CSCS=\"GSM\"\r";


//const uint8_t ATQSIMSTAT[] = "AT+QSIMSTAT\r";
/*<enable> Indicates whether to show an unsolicited event code that indicates whether the SIM has
been inserted or removed
0 Switch off detecting SIM card
1 Switch on detecting SIM card
<insert_status> Indicates whether SIM card has been inserted
0 Low level of pin indicates SIM card is present
1 High level of pin indicatesSIM is present*/

//const uint8_t ATQNSTATUS[] = "AT+QNSTATUS\r";
/*<status> 255 Not ready to retrieve network status
0 Work in normal state
1 No available cell
2 Only limited service is available*/

//const uint8_t ATCSQ[] = "AT+CSQ\r";
/*<rssi> 0 -113 dBm or less
1 -111 dBm
2...30 -109... -53 dBm
31 -51 dBm or greater
99 Not known or not detectable
<ber> (in percent):
0...7 As RXQUAL values in the table in GSM 05.08 subclause 8.2.4
99 Not known or not detectable*/

typedef enum
{
	GSM_LINE_NO_HEAD = 0,
	GSM_LINE_HEAD_L = 1,
	GSM_LINE_HEAD_H = 2,
	GSM_LINE_TAIL_L = 3,
	GSM_LINE_TAIL_H = 4,
}GSM_LINE_TYPE;
typedef enum
{
	AT_SEND = 0,
	AT_RETRY_WAIT = 1
}AT_UART_STATE;
AT_UART_STATE E_at_gsm_state = AT_SEND;

typedef struct 
{
    CIRC_BUFF_TYPE *const buffer;
    uint8_t head;
    uint8_t tail;
    const uint8_t maxlen;
}CIRC_BUFF_ST;
CIRC_BUFF_TYPE GSM_Rx_buff[MAX_CIRC_BUFF_DEPTH];

#ifdef DEBUG_ENABLE
extern uint8_t u8_log_uart_tx_flag; 
#endif
extern uint8_t h_u8GSM_tx_flag,h_u8GSM_rx_flag;
GSM_LINE_TYPE line_ptr = GSM_LINE_HEAD_L;
extern int8_t h_u8GSM_End_cnt;
static char date_time_arr[GSM_TIME_LEN] = {0};
static uint8_t u8AT_array[MAX_CUSTOM_AT_SIZE] = {0};
uint8_t u8at_index = 0;
char *u8msg_data_ptr = NULL;
uint8_t u8_at_retry_cnt = 0;
uint8_t u8GSM_uart_err_cnt = 0;
uint8_t h_u8GSM_timer1_cnt = 0;
 
AT_CALLBACK cb = NULL;
AT_CALLBACK Tout_cb = NULL;

CIRC_BUFF_ST Rx_GSM_Circ = {GSM_Rx_buff,0,0,MAX_CIRC_BUFF_DEPTH};


 G_RESP_CODE h_u8GSM_buf_put(CIRC_BUFF_ST *circ_buff,CIRC_BUFF_TYPE *msg_val);
 G_RESP_CODE h_u8GSM_buf_get(CIRC_BUFF_ST *circ_buff,CIRC_BUFF_TYPE *msg_val);
 G_RESP_CODE h_u8GSM_buf_chk(CIRC_BUFF_ST *circ_buff);
 void h_u8GSM_CIRCseek(CIRC_BUFF_ST *circ_buff,uint8_t offset);
 
 int8_t h_u8GSM_CIRCsearch(CIRC_BUFF_ST *circ_buff,CIRC_BUFF_TYPE *str,uint8_t len);
 int8_t h_u8GSM_CIRCcopy(CIRC_BUFF_ST *circ_buff,uint8_t index,CIRC_BUFF_TYPE *str,uint8_t len);
 void h_u8GSM_cb_rx_end(void);
 
static G_RESP_CODE h_u8GSM_resp_hdlr(uint8_t param);
static G_RESP_CODE h_u8GSM_uart_send(uint8_t *tx_buff,uint16_t len,uint8_t line_end_cnt);
static G_RESP_CODE h_u8GSM_uart_handler(uint8_t *tx_buff,uint16_t len,uint8_t line_end_cnt,AT_CALLBACK gsm_cb);
G_RESP_CODE h_u8GSM_time_update_cb(uint8_t param);


 
G_RESP_CODE h_u8GSM_SelfTest(void)
{
	G_RESP_CODE ret = WAITING; 	 
	switch(u8at_index)
	{
		case 0:
		{			 
			ret = h_u8GSM_uart_handler(ATCPIN,sizeof(ATCPIN),1,h_u8GSM_resp_hdlr);  //+CME ERROR - 10 -> SIM NOT INSERTED	
			if(WAITING != ret)
			{
				if((ret == GSM_INT_ERROR)||(FAILURE == ret))
				{				 
					ret = GSM_NO_SIM;
					 
				}
				else if(ret == SUCCESS)
				{
					ret = GSM_SIM_OK;
					 
				}
				else
				{
					 
				}
				u8at_index = 0;
				h_u8GSM_CIRCflush();
			}						 
			break;
		}
		default:
		break;
	}
	if(SUCCESS == ret)
	{
		ret = WAITING;
	}
	return ret;
}
G_RESP_CODE h_u8GSM_init(void)
{
	G_RESP_CODE ret = WAITING; 	 
	switch(u8at_index)
	{
		case 0:
		{			
			h_u8GSM_timer1_cnt++;
			if(h_u8GSM_timer1_cnt > GSM_INIT_WAIT_INTERVAL)
			{
				h_u8GSM_timer1_cnt = 0;
				u8at_index = 1;
			}
			break;
		}
		case 1:
		{
			ret = h_u8GSM_uart_handler(ATIPR,sizeof(ATIPR),1,h_u8GSM_resp_hdlr);  
			if(ret != WAITING)
			{
				u8at_index = 2;				 
			}			 
			break;
		}
		case 2:
		{
			ret = h_u8GSM_uart_handler(ATE0,sizeof(ATE0),1,h_u8GSM_resp_hdlr);  
			if(ret != WAITING)
			{
				u8at_index  =3;				 
			}			 
			break;			 			
		}
		case 3:
		{
			ret = h_u8GSM_uart_handler(ATCPIN,sizeof(ATCPIN),1,h_u8GSM_resp_hdlr);  //+CME ERROR - 10 -> SIM NOT INSERTED	
			if(WAITING != ret)
			{
				if((ret == GSM_INT_ERROR)||(FAILURE == ret))
				{				 
					ret = GSM_NO_SIM;					 
				}
				else if(ret == SUCCESS)
				{
					
					 
				}
				else
				{
					 
				}
				u8at_index = 4;
			}			
			break;
		}
		case 4:
		{
			ret = h_u8GSM_uart_handler(ATQMOSTAT,sizeof(ATQMOSTAT),1,h_u8GSM_resp_hdlr);  
			if(ret != WAITING)
			{
				u8at_index = 5;
			}
			break;
		}
		case 5:
		{
			ret = h_u8GSM_uart_handler(ATCMGF,sizeof(ATCMGF),1,h_u8GSM_resp_hdlr);   //+CME ERROR - 3517 -> SIM NOT READY
			if(ret != WAITING)
			{
				u8at_index = 6;
			}
			else
			{
				 
			}
			break;
		}
		case 6:
		{
			ret = h_u8GSM_uart_handler(ATCSCS,sizeof(ATCSCS),1,h_u8GSM_resp_hdlr);			 
			if(ret != WAITING)
			{
				u8at_index = 7;
			}
			break;
		}
		case 7:
		{
			ret = h_u8GSM_uart_handler(ATCTZU,sizeof(ATCTZU),1,h_u8GSM_resp_hdlr);			 
			if(ret != WAITING)
			{
				u8at_index = 8;
			}
			break;
		}		
		case 8:
		{
			//ret = h_u8GSM_uart_handler(ATCMGD,sizeof(ATCMGD),1,h_u8GSM_resp_hdlr);	
			ret = h_u8GSM_uart_send(ATCMGD,sizeof(ATCMGD),1);
			if(ret != WAITING)
			{
				u8at_index = 9;				 				 
			}						
			break;
		}	
		case 9:
		{
			h_u8GSM_CIRCflush();
			h_u8GSM_time_update();
			u8at_index = 0;
			ret = GSM_OK;
			break;
		}
		default:
		break;
	}
	if(SUCCESS == ret)
	{
		ret = WAITING;
	}
	return ret;
}
 
G_RESP_CODE h_u8GSM_record_del(void)
{
	G_RESP_CODE ret = WAITING; 
	ret = h_u8GSM_uart_handler(ATQFDEL,sizeof(ATQFDEL),1,h_u8GSM_resp_hdlr); 	 
	return ret;
}
G_RESP_CODE h_u8GSM_CallPlayBack(void)
{
	G_RESP_CODE ret = WAITING; 
	ret = h_u8GSM_uart_handler(ATQPSND,sizeof(ATQPSND),1,h_u8GSM_resp_hdlr); 
	return ret;
}
G_RESP_CODE h_u8GSM_record_start(void)
{
	G_RESP_CODE ret = WAITING; 
	ret = h_u8GSM_uart_handler(ATQAUDRD,sizeof(ATQAUDRD),1,h_u8GSM_resp_hdlr); 	 
	return ret;
}

G_RESP_CODE h_u8GSM_record_stop(void)
{
	G_RESP_CODE ret = WAITING; 
	ret = h_u8GSM_uart_handler(ATQAUDRDS,sizeof(ATQAUDRDS),1,h_u8GSM_resp_hdlr); 	 
	return ret;
}
G_RESP_CODE h_u8GSM_call_hang(void)
{
	G_RESP_CODE ret = WAITING; 
	ret = h_u8GSM_uart_handler(ATH,sizeof(ATH),1,h_u8GSM_resp_hdlr); 	 
	//ret = h_u8GSM_uart_send(ATH,sizeof(ATH),1);
	return ret;
}
G_RESP_CODE h_u8GSM_sms_send(uint8_t *phone_no,const uint8_t *data)
{
	G_RESP_CODE ret = WAITING; 
	uint16_t len = 0;
	uint8_t send_key = CTRLZ,k = 0;
	uint8_t *ptr = u8AT_array;
	uint8_t send_arr[] = "AT+CMGS=\"+91";
	switch(u8at_index)
	{
		case 0:
		{
			if(strlen((char *)phone_no) > (PHONE_NO_OFFSET-1) || (strlen((char *)phone_no) < 1))
			{				
				ret = INVALID_PARAM;
				break;
			}
			else if(strlen((char *)data) <= 0)
			{
				ret = INVALID_PARAM;
				break;
			}
			else
			{
				/*for(len=0;len<MAX_CUSTOM_AT_SIZE;len++)
				{
					u8AT_array[len]='\0';
				}*/
				u8GL_strclear(u8AT_array,MAX_CUSTOM_AT_SIZE);
				//sprintf((char *)u8AT_array,"AT+CMGS=\"+91%s\"\r",phone_no);
				k =  u8GL_ncopy(ptr,send_arr,12);
			        ptr = ptr + k;
				k =  u8GL_ncopy(ptr,phone_no,PHONE_NO_OFFSET);
			        ptr = ptr + k;
			       *ptr = '"';
			        ptr++;
				*ptr = '\r';
			        ptr++;
				*ptr = '\0';
			        ptr++;
				
				len = strlen((char *)u8AT_array);
				ret = h_u8GSM_uart_send(u8AT_array,len,0);
				if(ret == SUCCESS)
				{
					ret = WAITING;
					u8at_index = 1;
				}	
				else
				{
					break;	
				}
			}			
		}
		case 1:
		{
			len = strlen((char *)data);
			ret = h_u8GSM_uart_send(data,len,0);
			if(ret == SUCCESS)
			{
				ret = WAITING;
				u8at_index = 2;
			}
			else
			{
				break; 
			}			
			
			/*********break intentionally left*************/
		}
		case 2:
		{
			ret = h_u8GSM_uart_send(&send_key,1,0);
			u8at_index = 0;			 
			break;
		}
		default:
		break;
	}	
	return ret;
}

G_RESP_CODE h_u8GSM_call_send(uint8_t *phone_no)
{
	G_RESP_CODE ret = WAITING;
	int i;	 
	uint16_t len = 0;
	uint8_t send_arr[] = "ATD+91";
	uint8_t *ptr = u8AT_array;
	uint8_t k = 0;
	switch(u8at_index)
	{
		case 0:
		{
			if(strlen((char *)phone_no) > (PHONE_NO_OFFSET-1) || (strlen((char *)phone_no) < 1))
			{
				ret = INVALID_PARAM;
				break;
			}	
			else
			{
				/*for(i=0;i<MAX_CUSTOM_AT_SIZE;i++)
				{
					u8AT_array[i]='\0';
				}*/
				u8GL_strclear(u8AT_array,MAX_CUSTOM_AT_SIZE);
				//sprintf((char *)u8AT_array,"ATD+91%s;\r",phone_no);	
				k =  u8GL_ncopy(ptr,send_arr,6);
			        ptr = ptr + k;
				k =  u8GL_ncopy(ptr,phone_no,PHONE_NO_OFFSET);
			        ptr = ptr + k;				
			       *ptr = ';';
			        ptr++;
			       *ptr = '\r';
			        ptr++;
			       *ptr = '\0';
			        ptr++;
				u8at_index = 1;				
			}	
			/*****break intentionally leftout**********/
		}
		case 1:
		{
			len = strlen((char *)u8AT_array)+1;
			ret = h_u8GSM_uart_handler(u8AT_array,len,1,h_u8GSM_resp_hdlr);
			if(ret != WAITING)
			{
				u8at_index = 0;
			}
			break;
		}
		default:
		break;
	}
	
	return ret;
}

void h_u8GSM_at_reset(void)
{
	E_at_gsm_state = AT_SEND;
	u8_at_retry_cnt = 0;
	h_u8GSM_timer1_cnt = 0;
	u8at_index  =0;
	h_u8GSM_CIRCflush();
	h_u8GSM_tx_flag = INT_IDLE;
	h_u8GSM_rx_flag = INT_IDLE;
	h_u8GSM_End_cnt = 0;
	line_ptr = GSM_LINE_HEAD_L;
}

static G_RESP_CODE h_u8GSM_uart_send(uint8_t *tx_buff,uint16_t len,uint8_t line_end_cnt)
{
	G_RESP_CODE ret = GSM_UART_ERROR;
	MD_STATUS status = MD_OK;
	h_u8GSM_End_cnt = 0;
	line_ptr = GSM_LINE_HEAD_L;
	h_u8GSM_tx_flag = INT_IDLE;
	timeout_start(GSM_TIMEOUT_VAL);
	status = R_UART0_Send(tx_buff,len);
	if(MD_ARGERROR != status)
	{		    
		while((!h_u8GSM_tx_flag)&&(!is_timeout_expired()));
		if(NOT_EXPIRED == is_timeout_expired())
		{
			ret = SUCCESS;
		}			
	}		 
	h_u8GSM_tx_flag = INT_IDLE;
	timeout_stop();
	
	
	
	h_u8GSM_rx_flag = INT_IDLE;
	if(line_end_cnt == 0)
	{
		h_u8GSM_rx_flag = INT_CMPL;
	}				
	h_u8GSM_End_cnt = line_end_cnt;		 
	timeout_start(GSM_TIMEOUT_VAL);
	while((!h_u8GSM_rx_flag)&&(!is_timeout_expired()));
	if(EXPIRED == is_timeout_expired())
	{
		ret = GSM_UART_ERROR;
	}	
	timeout_stop();
	h_u8GSM_rx_flag = INT_IDLE;
	
	if(ret == GSM_UART_ERROR)
	{
		//m_u8MSGQ_cmd_send(LCD_DBG_CMD,2,FP_QUEUE);
		u8GSM_uart_err_cnt++;
		if(u8GSM_uart_err_cnt > MAX_AT_TIMEOUT_CNT)
		{					 
			if(Tout_cb != NULL)					 					
			{
				ret = (*Tout_cb)(0);
				if(ret != MSGQ_FULL)
				{
					u8GSM_uart_err_cnt = 0;
				}				
			}
		}
	}	 
	return ret;
}
static G_RESP_CODE h_u8GSM_uart_handler(uint8_t *tx_buff,uint16_t len,uint8_t line_end_cnt,AT_CALLBACK gsm_cb)
{
	G_RESP_CODE ret = WAITING;
	switch(E_at_gsm_state)
	{
		case AT_SEND:
		{
			ret = h_u8GSM_uart_send(tx_buff,len,line_end_cnt);
			if(ret != GSM_UART_ERROR)
			{
				ret = gsm_cb(NILL);		 
			}
			else
			{
								
			}
			if(ret != SUCCESS)
			{
				u8_at_retry_cnt++;
				E_at_gsm_state =  AT_RETRY_WAIT;			 
				if(u8_at_retry_cnt >= MAX_AT_RETRY_CNT)
				{
					u8_at_retry_cnt = 0;
					//ret = FAILURE;
					E_at_gsm_state = AT_SEND;
				}
				else
				{
					ret = WAITING;
				}			
			}
			else
			{
				u8GSM_uart_err_cnt  =0;
			}
			 			
			break;
		}
		case AT_RETRY_WAIT:
		{
			h_u8GSM_timer1_cnt++;
			if(h_u8GSM_timer1_cnt > AT_RETRY_WAIT_INT)
			{
				h_u8GSM_timer1_cnt = 0;
				E_at_gsm_state = AT_SEND;
				h_u8GSM_CIRCflush();				 
			}
			break;
		}
		default:
		break;
	}
	return ret;
}

G_RESP_CODE h_u8GSM_time_update(void)
{
	G_RESP_CODE ret = WAITING;	 
	ret = h_u8GSM_uart_handler(ATCCLK,sizeof(ATCCLK),2,h_u8GSM_time_update_cb);
	if(ret == FAILURE)
	{
		;
	}	
	return ret;
}
void h_u8GSM_time_get(char *time_arr)
{
	int i = 0;
	for(i = 0;i < GSM_TIME_LEN;i++)
	{
		*time_arr = date_time_arr[i];
		time_arr++;
	}
}
G_RESP_CODE h_u8GSM_time_update_cb(uint8_t param)
{
	G_RESP_CODE ret = SUCCESS;
	int8_t head = -1;
	 
	 
	 
	
	head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"ERROR",5);
	if(head == -1)
	{
		head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"+CCLK:",6);
		if(head != -1)
		{
			h_u8GSM_CIRCcopy(&Rx_GSM_Circ,head+8,date_time_arr,17);
			h_u8GSM_CIRCseek(&Rx_GSM_Circ,39);
		}
	}
	else
	{
		h_u8GSM_CIRCflush();
	}
	return ret;
}
static G_RESP_CODE h_u8GSM_resp_hdlr(uint8_t param)
{
	//uint16_t cnt=0;
	uint8_t state = 0;
	int8_t head = -1;
	G_RESP_CODE ret = FAILURE;
 

	ret = h_u8GSM_Q_chk(GSM_BUFF_Q);
	if(MSGQ_EMPTY != ret)
	{	
		ret = FAILURE;
		switch(state)
		{
			case 0:
			{
				head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"OK",2);
				if(head != -1)
				{
					ret =  SUCCESS;
					break;
				}
				state = 1;
			}
			case 1:
			{
				head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"ERROR",5);
				if(head != -1)
				{
					ret =  GSM_INT_ERROR;
					break;
				}
				state = 2;			 
			}
			case 2:
			{
				head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"READY",5);
				if(head != -1)
				{
					ret =  SUCCESS;				 
				}			
				break;
			}
			default:		 
			break;
		}
		h_u8GSM_CIRCflush();
	}	 
	return ret;
}
G_RESP_CODE h_u8GSM_uart_test(void)
{
	G_RESP_CODE ret = WAITING; 
	ret = h_u8GSM_uart_handler(AT,sizeof(AT),1,h_u8GSM_resp_hdlr); 	 
	return ret;
}
G_RESP_CODE h_u8GSM_call_resp_hdlr(void)
{
	G_RESP_CODE ret  = WAITING;	
	uint8_t state = 0;
	int8_t head = -1;	 
	ret = h_u8GSM_Q_chk(GSM_BUFF_Q);
	if(MSGQ_EMPTY != ret)
	{
		ret  = WAITING;
		switch(state)
		{
			case 0:
			{
				head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"MO RING",7);
				if(head != -1)
				{
					ret = CALL_ESTB;
					break;
				}				 
			}
			case 1:
			{
				head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"MO CONN",7);
				if(head != -1)
				{
					ret = h_u8GSM_uart_send(ATQPSND,sizeof(ATQPSND),1);
					if(ret != SUCCESS)
					{
						#ifdef DEBUG_ENABLE				 
						sprintf(debug_arr,"QPSND_ERROR\r");
						R_UART1_Send((uint8_t *)debug_arr,DEBUG_LEN);
					    	while(!u8_log_uart_tx_flag);
					   	u8_log_uart_tx_flag = 0;
						#endif
						ret = CALL_ANS_ERROR;
					}
					else
					{
						ret = CALL_ANS;	
					}
					break;
				}				
			}
			case 2:
			{
				head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"NO CARR",7);
				if(head != -1)
				{
					ret = CALL_HANG;
					break;
				}				
			}
			case 3:
			{
				head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"OK",2);
				if(head != -1)
				{
					//ret =  SUCCESS;
					break;
				}								 
			}
			case 4:
			{
				head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"ERROR",2);
				if(head != -1)
				{
					//ret =  FAILURE;
					break;
				}				
				break;
			}
			default:
			break;
		}
	}
	else
	{
		ret = WAITING;
	}
	return ret;
}
G_RESP_CODE sms_resp_handler(uint8_t param)
{	 
	G_RESP_CODE ret  = WAITING;	
	uint8_t state = 0;
	int8_t head = -1;
	 
	ret = h_u8GSM_Q_chk(GSM_BUFF_Q);
	if(MSGQ_EMPTY != ret)
	{
		ret  = WAITING;
		switch(state)
		{
			case 0:
			{
				head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"ERROR",5);
				if(head != -1)
				{
					ret = FAILURE;					 
		                        m_u8MSGQ_cmd_send(ICON_STS,MSMS_FAIL,FP_QUEUE);
					break;
				}				 
			}
			case 1:
			{
				head = h_u8GSM_CIRCsearch(&Rx_GSM_Circ,"CMGS",4);
				if(head != -1)
				{
					ret = SUCCESS;					 
					m_u8MSGQ_cmd_send(ICON_STS,MSMS_SUCC,FP_QUEUE);					 
					break;
				}
			}
			default:
			break;
		}
	}
	else
	{
		ret  = WAITING;
	}
	return ret;	
}
G_RESP_CODE h_u8GSM_ctl(G_CTL_CODES ctl)
{
	G_RESP_CODE ret = SUCCESS;
	switch(ctl)
	{
		case ON:
		{
			if(ON == GSM_LDO_ENABLE)
			{
				ret = FAILURE;
			}
			else
			{
				GSM_LDO_ENABLE = ON;
			}			
			break;
		}
		case OFF:
		{
			if(OFF == GSM_LDO_ENABLE)
			{
				ret = FAILURE;
			}
			else
			{
				GSM_LDO_ENABLE = OFF;
			}
			break;
		}
		default:
		break;
	}
	return ret;
}
G_RESP_CODE h_u8GSM_PwrKeyCtl(G_CTL_CODES ctl)
{
	G_RESP_CODE ret = SUCCESS;
	switch(ctl)
	{
		case ON:
		{
			GSM_PWR_KEY = ON;
			if(ON == GSM_PWR_KEY)
			{
				ret = FAILURE;
			}
			else
			{
				
			}			
			break;
		}
		case OFF:
		{
			if(OFF == GSM_PWR_KEY)
			{
				ret = FAILURE;
			}
			else
			{
				GSM_PWR_KEY = OFF;
			}
			break;
		}
		default:
		break;
	}
	return ret;
}
 
void h_u8GSM_PowSeq(void)
{
    h_u8GSM_ctl(OFF);
    h_u8GSM_ctl(ON);
    
    h_vDEL_Ms(100);
    h_u8GSM_PwrKeyCtl(OFF);
    h_vDEL_Ms(100);
    h_u8GSM_PwrKeyCtl(ON);
    //h_vDEL_Ms(1000);
    //h_u8GSM_PwrKeyCtl(OFF);
	
}
void h_u8GSM_AT_TIMEOUT_CbRegister(AT_CALLBACK gsm_cb)
{
	Tout_cb = gsm_cb;
}
G_RESP_CODE h_u8GSM_Q_send(GSM_Q_NAME qname,CIRC_BUFF_TYPE *val)
 {
	 G_RESP_CODE ret  = SUCCESS;
	 switch(line_ptr)
	 {
		case GSM_LINE_HEAD_L:
		{
			if(*val == '\r')
			{
				line_ptr = GSM_LINE_HEAD_H;
			}
			break;
		}
		case GSM_LINE_HEAD_H:
		{
			if(*val == '\n')
			{
				line_ptr = GSM_LINE_TAIL_L;
			}
			break;
		}
		case GSM_LINE_TAIL_L:
		{
			if(*val == '\r')
			{
				line_ptr = GSM_LINE_TAIL_H;
			}
		        break;
		}
		case GSM_LINE_TAIL_H:
		{
			if(*val == '\n')
			{
				line_ptr = GSM_LINE_HEAD_L;
				h_u8GSM_End_cnt--;
			        if(h_u8GSM_End_cnt <=0)
			        {				      
				      h_u8GSM_cb_rx_end();
				      h_u8GSM_End_cnt = 0;
			        }				
			}			
			break;
		}
		default:
		break;
	 }
	 h_u8GSM_buf_put(&Rx_GSM_Circ,val);
	 return ret;
 }
 G_RESP_CODE h_u8GSM_Q_chk(GSM_Q_NAME qname)
 {
	 G_RESP_CODE ret  = WAITING;
	 ret = h_u8GSM_buf_chk(&Rx_GSM_Circ);
	 return ret;
 }
 G_RESP_CODE h_u8GSM_buf_put(CIRC_BUFF_ST *circ_buff,CIRC_BUFF_TYPE *msg_val)
 {
	G_RESP_CODE ret = SUCCESS;
	uint8_t temp_head = 0;
	temp_head = circ_buff->head;
	circ_buff->buffer[temp_head] = *msg_val;
	if((temp_head+1) >= circ_buff->maxlen)
	{
		if(circ_buff->tail == 0)
		{
			return MSGQ_FULL;
		}
		temp_head = 0;
	}
	else
	{
		if(circ_buff->tail == (temp_head+1))
		{
			return MSGQ_FULL;
		}
		temp_head = temp_head + 1;
	}
	//circ_buff->buffer[1].data[3] = 't';  for reference
	circ_buff->head = temp_head;
	return ret;
 }
 G_RESP_CODE h_u8GSM_buf_get(CIRC_BUFF_ST *circ_buff,CIRC_BUFF_TYPE *msg_val)
 {
	G_RESP_CODE ret = SUCCESS;
	uint8_t temp_tail = 0;
	temp_tail = circ_buff->tail;
	if((temp_tail - circ_buff->head) == 0)
	{
		return MSGQ_EMPTY;
	}
	*msg_val = circ_buff->buffer[temp_tail];
	if((temp_tail+1) >= circ_buff->maxlen)
	{
		temp_tail = 0;
	}
	else
	{
		temp_tail = temp_tail + 1;
	}
	circ_buff->tail = temp_tail;
	return ret;
 }
 G_RESP_CODE h_u8GSM_buf_chk(CIRC_BUFF_ST *circ_buff)
 {
	G_RESP_CODE ret = WAITING;	 
	if((circ_buff->tail - circ_buff->head) == 0)
	{
		return MSGQ_EMPTY;
	}
	return ret;
 }
void h_u8GSM_CIRCflush(void)
{
	Rx_GSM_Circ.tail = Rx_GSM_Circ.head;
}
int8_t h_u8GSM_CIRCcopy(CIRC_BUFF_ST *circ_buff,uint8_t index,CIRC_BUFF_TYPE *str,uint8_t len)
{
    int8_t ret = -1;
    uint8_t t4 = 0;
    uint8_t i = 0,r = 0;
   
    if(circ_buff->tail - circ_buff->head)
    {
	if(index >= circ_buff->maxlen)
        {
            index = index - circ_buff->maxlen;
        }
        if(circ_buff->head > circ_buff->tail)
        {
            if((index < circ_buff->tail)||(index >= circ_buff->head))
            {
                return -1;
            }
            t4 = circ_buff->head;
        }
        else
        {
            if((index < circ_buff->tail)&&(index >= circ_buff->head))
            {
                return -1;
            }
            t4 = circ_buff->maxlen;
        }
        for(i = index;i<t4;i++)
        {
             str[r] = circ_buff->buffer[i];
             r++;
             if(r == len)
             {
                 str[r] = '\0';
                  ret = i;
                  break;
             }
            if(i == circ_buff->maxlen-1)
            {
                i = -1;
                t4 = circ_buff->head;
            }
        }
    }
    str[r] = '\0';
    return ret;
}
int8_t h_u8GSM_CIRCsearch(CIRC_BUFF_ST *circ_buff,CIRC_BUFF_TYPE *str,uint8_t len)
{
    int8_t ret = -1;
    uint8_t t1 = 0,t4 = 0;
    uint8_t i = 0,r = 0;
    t1 = circ_buff->tail;
    if(circ_buff->tail - circ_buff->head)
    {
        if(circ_buff->head > circ_buff->tail)
        {
            t4 = circ_buff->head;
        }
        else
        {
            t4 = circ_buff->maxlen;
        }
        for(i = t1;i<t4;i++)
        {
            if(str[r] == circ_buff->buffer[i])
            {
                r++;
                if(r == len)
                {
                    ret = i;
                    break;
                }
            }
            else
            {
                if(r > 0)
                {
                    i--;
                }
                r = 0;
            }
            if(i == circ_buff->maxlen-1)
            {
                i = -1;
                t4 = circ_buff->head;
            }
            
        }
        if(ret != -1)
        {
            ret = ret - len + 1;
            if(ret<0)
            {
                ret = circ_buff->maxlen-(ret* -1);
            }
        }
    }
    
    return ret;
}
void h_u8GSM_CIRCseek(CIRC_BUFF_ST *circ_buff,uint8_t offset)
{
    if(circ_buff->head - circ_buff->tail)
    {
        offset = (circ_buff->tail + offset) % circ_buff->maxlen;
        if(circ_buff->head > circ_buff->tail)
        {
            if((offset < circ_buff->tail)||(offset >= circ_buff->head))
            {
                offset = circ_buff->head;
            }
        }
        else
        {
            if((offset<circ_buff->tail)&&(offset >= circ_buff->head))
            {
                offset = circ_buff->head;
            }
        }
        circ_buff->tail = offset;
    }
}
void h_u8GSM_cb_rx_end(void)
{
	h_u8GSM_rx_flag = INT_CMPL;
}