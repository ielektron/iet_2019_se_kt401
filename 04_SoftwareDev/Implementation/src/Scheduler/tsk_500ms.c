/** \file tsk_500ms.c
 * \brief This module contains 500 ms periodic tasks.
 *
 *  This module provides the functionality related to tasks that are scheduled
 *  every 500 ms.  Functions that need to be ran in this interval can be added to
 *  task in this module.  The main function will call this task at the pre-
 *  determined interval and all the code within it.  If the task needs to be
 *  ran continuously it should be placed in tsk().
 */
/******************************************************************************
* Includes
*******************************************************************************/
#include "A_main_app_handler.h" 
#include "H_keypad_driver.h"
#include "M_ZONE_fire_hdlr.h" 
#include "H_lcd_driver.h"
#include "A_UI_handler.h" 
#include "M_ADC_hdlr.h"
#include "M_power_handler.h"
#include "M_rtc_handler.h" 
#include "M_eeprom_handler.h" 
/******************************************************************************
* Constants
*******************************************************************************/

/******************************************************************************
* Typedefs
*******************************************************************************/

/******************************************************************************
* Variable Definitions
*******************************************************************************/

/******************************************************************************
* Function Prototypes
*******************************************************************************/

/******************************************************************************
* Function Definitions
*******************************************************************************/
/********************************************************************************
* Function : Tsk_100ms
*//**
* \section Description Description:
*
*  This function is used for the 100 ms tasks.
*
* \param        None.
* \return       None.
*
* \see Tsk
* \see Tsk_10ms
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date    Software Version    Initials   Description
*         0.1.1                  Function Created.
*
*******************************************************************************/
void Tsk_500ms(void)
{   
   M_ADCMonitor();   
   a_u8APP_handler();   
   m_POW_monitor();
   ZONE_FIRE_monitor();
   E2P_retryHandler();
   #ifdef RTC_RETRY_ENABLE
 	RTC_retryHandler();
   #endif
   
}

/*** End of File **************************************************************/

