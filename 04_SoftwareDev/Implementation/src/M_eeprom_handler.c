#include "M_eeprom_handler.h" 
#include "r_cg_serial.h"
#include "A_UI_handler.h" 
#include "M_GeneralLib.h"
#include "M_event_handler.h"
#include "M_rtc_handler.h"
#include "M_ZONE_intr_hdlr.h"
#include "M_ZONE_fire_hdlr.h"
#include "M_GSM_interface_hdlr.h"
#include "M_time_out.h"
 


#define E2P_MAX_RETRY_CNT 5

#define E2P_CRI_WRITE_RETRY_FLAG 0x00
#define E2P_CRI_READ_RETRY_FLAG 0x01
#define E2P_LOG_WRITE_RETRY_FLAG 0x02
#define E2P_LOG_READ_RETRY_FLAG 0x03
#define E2P_LOG_FLAG 0x04

#define LOG_INIT 0
#define LOG_TRIG 1

extern uint8_t Log_uart_TxFlag;

uint8_t EWrite_DataBuffer[MAX_EEPROM_PAGE_SIZE] = {0};
 
static int16_t log_e2p_page = LOG_PAGE_START;
static uint8_t u8E2P_log_start = LOG_INIT;

static uint16_t E2P_eventFlag = 0x00;
 
uint8_t E2P_retryCount = 0;



uint8_t E2P_EventFlagManager(uint8_t flag_pos,uint8_t operation);

void E2P_retryHandler(void)
{
	G_RESP_CODE ret = SUCCESS;
	if(E2P_eventFlag != 0)
	{
		if(E2P_eventFlag & 0x01)
		{
			ret = E2P_criticalDataStore();
			if(ret == SUCCESS)
			{
				E2P_EventFlagManager(E2P_CRI_WRITE_RETRY_FLAG,FLAG_CLR);
				APP_eventManager(E_E2P_CRI_STORE,SUCCESS);
			}
			else
			{
				E2P_retryCount++;
				if(E2P_retryCount > E2P_MAX_RETRY_CNT)
				{
					E2P_retryCount = 0;
					E2P_EventFlagManager(E2P_CRI_WRITE_RETRY_FLAG,FLAG_CLR);
					APP_eventManager(E_E2P_CRI_STORE,FAILURE);
				}
			}
		}
		else if(E2P_eventFlag & 0x02)
		{
			ret = E2P_criticalDataRetrieve();
			if(ret == SUCCESS)
			{
				E2P_EventFlagManager(E2P_CRI_READ_RETRY_FLAG,FLAG_CLR);
				APP_eventManager(E_E2P_CRI_READ,SUCCESS);
			}
			else
			{
				E2P_retryCount++;
				if(E2P_retryCount > E2P_MAX_RETRY_CNT)
				{
					E2P_retryCount = 0;
					E2P_EventFlagManager(E2P_CRI_READ_RETRY_FLAG,FLAG_CLR);
					APP_eventManager(E_E2P_CRI_READ,FAILURE);
				}
			}
		}
		else if(E2P_eventFlag & 0x04)
		{
			ret = E2P_LogStore();
			if(ret == SUCCESS)
			{
				E2P_EventFlagManager(E2P_LOG_WRITE_RETRY_FLAG,FLAG_CLR);
			}
			else
			{
				E2P_retryCount++;
				if(E2P_retryCount > E2P_MAX_RETRY_CNT)
				{
					E2P_retryCount = 0;
					E2P_EventFlagManager(E2P_LOG_WRITE_RETRY_FLAG,FLAG_CLR);
				}
			}
		}
	}
}

G_RESP_CODE E2P_SendSYSdata(void)
{
	G_RESP_CODE ret = SUCCESS;
	UI_sysDATtoArray(EWrite_DataBuffer);
	ZONE_StsStore(EWrite_DataBuffer,0);
	GSM_CommandSend_Ext(E_E2P_SEND,DEFAULT,EWrite_DataBuffer,MAX_EEPROM_BUFF_SIZE);
	return ret;
}
G_RESP_CODE E2P_SendSYS_ALT_data(E_SYS_EVENTS command)
{
	G_RESP_CODE ret = SUCCESS;
	UI_sysDATtoArray(EWrite_DataBuffer);
	ZONE_StsStore(EWrite_DataBuffer,command);
	GSM_CommandSend_Ext(command,DEFAULT,EWrite_DataBuffer,MAX_EEPROM_BUFF_SIZE);
	return ret;
}
G_RESP_CODE E2P_ReceiveSYSdata(uint8_t *arr)
{
	G_RESP_CODE ret = SUCCESS;
	UI_sysDATtoStruct(arr);	
	E2P_CRI_STORE();
	return ret;
} 


G_RESP_CODE E2P_LOG_STORE(void)
{
	G_RESP_CODE ret = SUCCESS;
	/*	
	ret = RTC_update();
	if(ret == SUCCESS)
	{
		ret = E2P_LogStore();
		if(ret != SUCCESS)
		{
			E2P_EventFlagManager(E2P_LOG_WRITE_RETRY_FLAG,FLAG_SET);
		}
	}
	else
	{
		#ifdef RTC_RETRY_ENABLE
		E2P_EventFlagManager(E2P_LOG_FLAG,FLAG_SET);
		RTC_retryManager();	
		
		#else
		ret = E2P_LogStore();
		if(ret != SUCCESS)
		{
			E2P_EventFlagManager(E2P_LOG_WRITE_RETRY_FLAG,FLAG_SET);
		}
		#endif
	}
	*/
	return ret;
}

G_RESP_CODE E2P_LogStore(void)
{
	G_RESP_CODE ret = SUCCESS;	
	uint8_t k = 0;
	uint8_t *ptr = EWrite_DataBuffer,*temp_ptr = NULL;
	
	u8GL_strclear(EWrite_DataBuffer,MAX_EEPROM_PAGE_SIZE);
	
	*ptr = '$';
        ptr++;
	
	temp_ptr = RTC_LogStore(ptr);
	if(temp_ptr == NULL)
	{
		k =  u8GL_ncopy(ptr,(uint8_t *)"TIME ERROR",10);
		ptr = ptr + k;
	}
	else
	{
		ptr = temp_ptr;		
	}	
	*ptr = LOG_RTC_DL;
        ptr++;
	ptr = ZONE_LogStore(ptr);
	ptr = ZONE_FIRE_LogStore(ptr);
	*ptr = '\0';
	
	
	ret = h_u8EEPROM_page_write(EWrite_DataBuffer,M95M01_MAX_PAGE_SIZE,LOG);
	if(ret != EEPROM_SUCCESS)
	{
		ret = FAILURE;
	}
	else
	{	
		E2P_CRI_STORE();
		ret = SUCCESS;
	}	 
	return ret;
}
G_RESP_CODE E2P_LogRetrieve(uint8_t log_cmd)
{
	uint16_t u16Len = 0;
	uint8_t FL_ret = WAITING,i =0,j = 0, k = 0;
	switch(log_cmd)
	{
		case LOG_START:
		{
			switch(u8E2P_log_start)
			{
				case LOG_INIT:
				{
					log_e2p_page = h_u8EEPROM_page_get();					 
					u8E2P_log_start = LOG_TRIG;
					/*****break intentionally left********/ 
				}
				case LOG_TRIG:
				{
					for(i = 0;i < 50;i++)
					{
						if((log_e2p_page-1) < LOG_PAGE_START)
						{
							log_e2p_page = LOG_PAGE_END;
						}
						else
						{
							log_e2p_page--;
						}
						if(h_u8EEPROM_page_get() == log_e2p_page)
						{
							log_e2p_page = LOG_PAGE_START;
							u8E2P_log_start = LOG_INIT;
							FL_ret = SUCCESS;
							u8GL_strclear(EWrite_DataBuffer,MAX_EEPROM_PAGE_SIZE);							 
							break;
						}
						else
						{
							h_u8EEPROM_page_read(EWrite_DataBuffer,log_e2p_page);
							u16Len = strlen((char *)EWrite_DataBuffer);
							if(u16Len!=0)
							{
								for(j = 0;j < 50;j++)
								{
									if(0xFF == EWrite_DataBuffer[j])
									{
										k++;
									}
								}
								if(k<20)
								{
									Log_uart_TxFlag = INT_IDLE;
									R_UART2_Send(EWrite_DataBuffer,u16Len);
									timeout_chk(&Log_uart_TxFlag,8,DEFAULT_TIMEOUT_VALUE);									 
								}
								k = 0;	
							}
						}					
					}
					break;
				}
				default:
				break;
			}					
			break;
		}
		case LOG_STP:
		{
			log_e2p_page = LOG_PAGE_START;
			u8E2P_log_start = LOG_INIT;
			FL_ret = SUCCESS;
			u8GL_strclear(EWrite_DataBuffer,MAX_EEPROM_PAGE_SIZE);
			break;
		}
		default:
		break;
	}	 
	return FL_ret;
}
G_RESP_CODE E2P_LogReset(void)
{
	G_RESP_CODE resp = EEPROM_SUCCESS;
	int i = 0;
	for(i = 0;i < MAX_EEPROM_BUFF_SIZE;i++)
	{
		EWrite_DataBuffer[i]=0xff;
		//eeprom_buff[i] = 0x00;
	}
	for(i = LOG_PAGE_START;i<=LOG_PAGE_END;i++)
	{
		resp = h_u8EEPROM_page_write((uint8_t *)EWrite_DataBuffer, MAX_EEPROM_PAGE_SIZE,LOG);
		if(resp != EEPROM_SUCCESS)
		{
			if(resp == WRITE_IN_PROGRESS)
			{
				
			}
			else
			{
				 
			}
			 
		}
	}	 
	return resp;
}
G_RESP_CODE E2P_CRI_STORE(void)
{
	G_RESP_CODE ret = SUCCESS;
	ret = E2P_criticalDataStore();
	if(ret != SUCCESS)
	{
		E2P_EventFlagManager(E2P_CRI_WRITE_RETRY_FLAG,FLAG_SET);
	}
	return ret;
}

G_RESP_CODE E2P_criticalDataStore(void)
{	 
	m95_ERROR_CODE resp;
	G_RESP_CODE ret = SUCCESS;
	uint16_t temp_page = 0;	
	 
	u8GL_strclear(EWrite_DataBuffer,MAX_EEPROM_PAGE_SIZE);
	
	UI_sysDATtoArray(EWrite_DataBuffer);
	
	RTC_currTimeToArray(EWrite_DataBuffer);
	
	temp_page = h_u8EEPROM_page_get();	
	EWrite_DataBuffer[CURR_E2P_PAGE] = (uint8_t)((temp_page & 0xff00)>>8);
	EWrite_DataBuffer[CURR_E2P_PAGE+1] = (uint8_t)(temp_page & 0x00ff);
	
	
	resp = h_u8EEPROM_page_write(EWrite_DataBuffer,MAX_EEPROM_PAGE_SIZE,DAT);
	if(resp != EEPROM_SUCCESS)
	{		 
		/***error need to be handled****/
		ret = FAILURE;
	}
	else
	{
		ret = SUCCESS;		 		 
	} 	
	return ret;
}
G_RESP_CODE E2P_CRI_RETRIEVE(void)
{
	G_RESP_CODE ret = SUCCESS;
	ret = E2P_criticalDataRetrieve();
	if(ret != SUCCESS)
	{
		E2P_EventFlagManager(E2P_CRI_READ_RETRY_FLAG,FLAG_SET);
	}
	return ret;
}
G_RESP_CODE E2P_criticalDataRetrieve(void)
{ 
	G_RESP_CODE ret = FAILURE;
	m95_ERROR_CODE resp;
	uint16_t temp_page = 0;
	 
	u8GL_strclear(EWrite_DataBuffer,MAX_EEPROM_BUFF_SIZE);
	resp = h_u8EEPROM_page_read(EWrite_DataBuffer,CRITICAL_DATA_PAGE);
	if(resp != EEPROM_SUCCESS)
	{
		ret = FAILURE;
	}
	else
	{
		UI_sysDATtoStruct(EWrite_DataBuffer);
		
		temp_page = (uint16_t)EWrite_DataBuffer[CURR_E2P_PAGE];
		temp_page = temp_page << 8;	 
		temp_page= (temp_page)| (uint16_t)EWrite_DataBuffer[CURR_E2P_PAGE+1];
		h_u8EEPROM_page_set(temp_page);
		
		ret = SUCCESS;		
	}	 
	return ret;
}
void E2P_eventManager(E_SYS_EVENTS event,uint8_t sts)
{
	switch(event)
	{
		case E_RTC_RETRY_STS:
		{
			if(FLAG_SET == E2P_EventFlagManager(E2P_LOG_FLAG,FLAG_CHK))
			{
				E2P_EventFlagManager(E2P_LOG_WRITE_RETRY_FLAG,FLAG_SET);
				E2P_EventFlagManager(E2P_LOG_FLAG,FLAG_CLR);
			}
			break;
		}
		default:
		break;
	}
}
uint8_t E2P_EventFlagManager(uint8_t flag_pos,uint8_t operation)
{
	uint8_t ret = 5;
	uint16_t chk_val = 0;
	switch(operation)
	{
	  	case FLAG_SET:
		{
			SETB(E2P_eventFlag,flag_pos);
			break;
		}
		case FLAG_CLR:
		{
			CLRB(E2P_eventFlag,flag_pos);
			break;
		}
		case FLAG_CHK:
		{
			chk_val = CHKB(E2P_eventFlag,flag_pos);
			if(chk_val)
			{
				ret = FLAG_SET;	
			}
			else
			{
				ret = FLAG_CLR;
			}
			break;
		}
		default:
		break;
	}
	return ret;
}



#ifdef STARTUP_E2P_CLEAR
void m_vE2P_clear(void)
{
	m95_ERROR_CODE resp;
	uint16_t temp_page;
	G_RESP_CODE ret;
	
	u8GL_strclear(EWrite_DataBuffer,MAX_EEPROM_PAGE_SIZE);
	 
	RTC_currTimeToArray(EWrite_DataBuffer);
	temp_page = LOG_PAGE_START;
	EWrite_DataBuffer[CURR_E2P_PAGE] = (uint8_t)((temp_page & 0xff00)>>8);
	EWrite_DataBuffer[CURR_E2P_PAGE+1] = (uint8_t)(temp_page & 0x00ff);
	
	resp = h_u8EEPROM_page_write(EWrite_DataBuffer,sizeof(EWrite_DataBuffer),DAT);
	if(resp != EEPROM_SUCCESS)
	{
		 
	}	 
}
#endif