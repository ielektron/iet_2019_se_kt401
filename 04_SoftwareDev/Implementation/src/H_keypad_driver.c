/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by IELEKTRON TECHNOLOGIES
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIESREGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*
* Copyright (C) 2017, ielektron technologies. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : keypad.c
* Version      : CodeGenerator for RL78/G13 V2.03.02.01 [15 May 2015]
* Device(s)    : R5F100FE
* Tool-Chain   : CA78K0R
* Description  : This file implements main function.
* Creation Date: 8/25/2017
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "H_keypad_driver.h"
 

const uint8_t key[4][3] = {               // defining characters //for keystrokes in Multidimensional Array
  {
    '1','2','3'  }
  ,   
  {
    '4','5','6'  }
  ,
  {
    '7','8','9'  }
  ,
  {
    '*','0','#'  }  
};
uint8_t u8keypad_read =0;
uint8_t col0 =0;
uint8_t col1 =0;
uint8_t col2 =0;
uint8_t u8CurKeyStatus =0;
uint8_t u8PrevKeyStatus =0;
uint8_t u8DebKey =0;
uint8_t u8DebCnt =0;
uint8_t send_key =  0;

 uint8_t u8_G_key_sts = RELEASED;
//static uint8_t keypad_read(void);

/***********************************************************************************************************************
* Function Name: keypad_read
* Description  : This function process of keypad.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/

void keypad_Handler(void)
{
 u8CurKeyStatus = keypad_read();
 if(u8CurKeyStatus!=0)
 {
  send_key = u8CurKeyStatus;
 } 
 else
 {
	 if(u8_G_key_sts == PRESSED)
	 {
		 u8_G_key_sts = RELEASED;
		 send_key = 0;		 
	 }
 }
}


/***********************************************************************************************************************
* Function Name: keypad_read
* Description  : This function process of keypad.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint8_t keypad_read(void)
{
 uint8_t row=0;
 uint8_t col=0;
 u8keypad_read=0;
 for(row=0;row <4;row++)
 {
  KEY_ROW0 =1;
  KEY_ROW1 =1;
  KEY_ROW2 =1;
  KEY_ROW3 =1;
  
	  if(row==0)
	  {
	   KEY_ROW0 =0;	  
	  }
	  else if (row==1)
	  {
	   KEY_ROW1 =0;  
	  }
	  else if (row==2)
	  {
	   KEY_ROW2 =0;  
	  }
	  else if (row==3)
	  {
	   KEY_ROW3 =0;	 
	  }
	  else
	  {
	  }
	for(col=0;col<3;col++)
	{
		col0 = KEY_COL0;
		col1 = KEY_COL1;
		col2 = KEY_COL2;
		if((KEY_COL0!=1) && (col==0))
		{
	         u8keypad_read = key[row][col];
		 //return (u8keypad_read);
		}
		else if((KEY_COL1!=1) && (col==1))
		{
		 u8keypad_read = key[row][col];
		 //return (u8keypad_read);
		}
		else if((KEY_COL2!=1) && (col==2))
		{
		 u8keypad_read = key[row][col];
		 //return (u8keypad_read);
		}
		else
		{
		}	
	
	}//end of col processing
  }//end of row processing
  return (u8keypad_read);
}

void get_key(KEY_TYPE_T *key)
{
	if((u8_G_key_sts == RELEASED)&&(send_key != '\0'))
	 {
		  	*key = send_key;
			send_key = '\0';
			u8_G_key_sts = PRESSED;
	 }
	 else
	 {
		 *key = '\0';
	 }	
}
KEY_TYPE_T KEY_rawGet(void)
{
	return u8CurKeyStatus;
}

KEY_TYPE_T KEY_stsGet(void)
{
	return u8_G_key_sts;
}