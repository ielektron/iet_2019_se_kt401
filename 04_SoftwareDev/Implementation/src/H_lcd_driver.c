///////////////////////////////////////////////////////////////////////////
////                             Lcd_2_16.c                            ////
////                  Driver for 2x16 LCD modules                      ////
////                                                                   ////
////  lcd_init()   Must be called before any other function.           ////
////                                                                   ////
////  lcd_putc(c)  Will display c on the next position of the LCD.     ////
////                     The following have special meaning:           ////
////                      \f  Clear display                            ////
////                      \n  Go to start of second line               ////
////                      \b  Move back one position                   ////
////                                                                   ////
////  lcd_gotoxy(x,y) Set write position on LCD (upper left is 1,1)    ////
////                                                                   ////
////  lcd_getc(x,y)   Returns character at position x,y on LCD         ////
////                                                                   ////
///////////////////////////////////////////////////////////////////////////
////                 (C) Copyright 2010 SiFe Engineering               ////
//// This source code may only be used by SiFe Engineering. This       ////
//// source code may be distributed to other only with written         ////
//// form SiFe Engineering.                                            ////
///////////////////////////////////////////////////////////////////////////

#include "r_cg_macrodriver.h"
#include "r_cg_port.h"
/* Start user code for include. Do not edit comment generated here */
#include "H_lcd_driver.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "r_cg_serial.h"
//#include ".\Code\global.h"
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"
#include "H_delay_driver.h"


//I/O defines   


//LCD ctrl config
#define LCD_WRITE_EN    0
#define LCD_READ_EN     1
//LCD Type
#define  LCD_TYPE       0x02           // 0=5x7, 1=5x10, 2=2 lines
#define  LCD_LINE_TWO   0x40    // LCD RAM address for the second line


#define BATTERY_INDEX 0
#define MSGQ_INDEX 1
#define MODE_CTRL_INDEX 2
#define DISABLE_INDEX 3
#define SIREN_INDEX 4

#define LCD_READ_INT 50//5 sec

uint8_t gui8LcdInitCmd[4] = { // These bytes need to be sent to the LCD to start it up.
                           	(0x20|(LCD_TYPE<<2)), // 0=5x7, 1=5x10, 2=2 lines
                           	LCD_CURSOR_OFF, 
                           	LCD_CLR_SCR, 
                           	LCD_ENT_MODE
                            };

const unsigned char s8_icon_data[E_ICON_END][8]={
{0x0E, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F},//BATT_NORM
{0x0E, 0x1B, 0x11, 0x11, 0x11, 0x11, 0x1F, 0x1F},//BATT_LOW
{0x0E, 0x1B, 0x15, 0x15, 0x11, 0x15, 0x11, 0x1F},//BATT_SHUT
{0x02, 0x1F, 0x08, 0x00, 0x11, 0x1B, 0x15, 0x11},//MSGQ_SEND
{0x01, 0x02, 0x14, 0x08, 0x11, 0x1B, 0x15, 0x11},//MSGQ_SUCC
{0x04, 0x04, 0x00, 0x04, 0x11, 0x1B, 0x15, 0x11},//MSGQ_FAIL
{0x08, 0x04, 0x02, 0x04, 0x08, 0x15, 0x15, 0x1D},//UI_CTRL
{0x0C, 0x0E, 0x0C, 0x0E, 0x08, 0x1E, 0x12, 0x1E},//KEY_CTRL
{0x0E, 0x0A, 0x0A, 0x1F, 0x1F, 0x1B, 0x1B, 0x1F},//DISABLE
{0x0A, 0x04, 0x0A, 0x04, 0x0E, 0x0A, 0x1F, 0x04},//SIREN_OFF
{0x05, 0x02, 0x05, 0x1C, 0x12, 0x11, 0x11, 0x1B},//NO_SIM
};
void lcd_writeCustomDesgn( char* cData, uint8_t ui8Loc);
void lcd_showCustomDesgn(uint8_t num,uint8_t ui8X, uint8_t ui8Y);
uint8_t lcd_recieveNibble();

char lcd_array[2][MAX_LCD_TXT_LEN+1] = {0};
//char print_array[17] = "updated0";
uint8_t debug_array[50] = {0};
uint8_t lcd_state = 0;
uint8_t lcd_read_retry = 0,lcd_write_retry = 0,lcd_update = 0;
uint8_t time2update = 0;
int lcd_tick = 0;

void show_icon(E_ICON_ARRAY icon_index,uint8_t col,uint8_t row)
{
	switch(icon_index)
	{
	case BATT_NORM:
	case BATT_LOW:	
	case BATT_SHUT:
	{
	lcd_writeCustomDesgn((char *)s8_icon_data[icon_index],BATTERY_INDEX);
	lcd_showCustomDesgn(BATTERY_INDEX,col,row);
	break;
	}
	case NO_SIM:
	case SMS_WAIT:
	case SMS_SUCC:	
	case SMS_FAIL:
	{
	lcd_writeCustomDesgn((char *)s8_icon_data[icon_index],MSGQ_INDEX);
	lcd_showCustomDesgn(MSGQ_INDEX,col,row);
	break;
	}
	case UI_CTRL:
	case KEY_CTRL:
	{
	lcd_writeCustomDesgn((char *)s8_icon_data[icon_index],MODE_CTRL_INDEX);
	lcd_showCustomDesgn(MODE_CTRL_INDEX,col,row);
	break;
	}
	case DISABLE:	
	{
	lcd_writeCustomDesgn((char *)s8_icon_data[icon_index],DISABLE_INDEX);
	lcd_showCustomDesgn(DISABLE_INDEX,col,row);
	break;
	}
	case SIREN_OFF:
	{
	lcd_writeCustomDesgn((char *)s8_icon_data[icon_index],SIREN_INDEX);
	lcd_showCustomDesgn(SIREN_INDEX,col,row);
	break;
	}
	default:
	lcd_goToXy(col,row);
	lcd_putC(' ');
	break;
	}
}
			 
void lcd_sendNibble(uint8_t ui8Char) 
{
   LCD_D1 = (bit)(ui8Char&0x01);
   LCD_D2 = (bit)((ui8Char>>1)&0x01);
   LCD_D3 = (bit)((ui8Char>>2)&0x01);
   LCD_D4 = (bit)((ui8Char>>3)&0x01);
   DelayUs(1);
   LCD_EN = 1;
   DelayUs(2);
   LCD_EN = 0;
   //ch-27
   DelayUs(2);
}


void lcd_sendByte(uint8_t ui8Addr, uint8_t ui8Data) 
{    
   DelayUs(1200);
   LCD_RS = ui8Addr;
   DelayUs(1);
   LCD_EN = 0;
   lcd_sendNibble(ui8Data >> 4);
   lcd_sendNibble(ui8Data & 0xf);
}

void lcd_init(void) 
{
   uint8_t ui8Cnt=0;
   
   LCD_RS = 0;
   LCD_EN = 0;
   //DelayMs(15);
   for(ui8Cnt = 0;ui8Cnt < MAX_LCD_TXT_LEN + 1;ui8Cnt++)
   {
	  lcd_array[0][ui8Cnt] =  ' ';
	  lcd_array[1][ui8Cnt] =  ' ';
   }
   ui8Cnt = 0;
   DelayUs(15000);
   for( ui8Cnt=1; ui8Cnt<=3; ++ui8Cnt ) 
   {
      lcd_sendNibble(3);
      //DelayMs(5);
      DelayUs(5000);
   }
   lcd_sendNibble(2);
   for( ui8Cnt=0; ui8Cnt<=3; ++ui8Cnt )
   {
      lcd_sendByte(0,gui8LcdInitCmd[ui8Cnt]);
   }
}

void lcd_goToXy(uint8_t ui8X, uint8_t ui8Y) 
{
   uint8_t ui8Addr=0;
   
   if(ui8Y != 1)
   {
     ui8Addr=LCD_LINE_TWO;
   }
   else
   {
     ui8Addr=0;
   }
   ui8Addr += ui8X-1;
   lcd_sendByte(0,0x80|ui8Addr);
}

void lcd_putC(char cChar)
{
   switch(cChar) 
   {
      case '\f': 
      {
         lcd_sendByte(LCD_CTRL_REG,LCD_CLR_SCR);
         //DelayMs(2);
	 DelayUs(2000);
      }
      break;
      
      case '\n': 
      {
         lcd_goToXy(1,2);        
      }
      break;
      
      case '\b': 
      {
         lcd_sendByte(LCD_CTRL_REG,LCD_CHAR_LEFT);  
      }
      break;
      
      default:
      {
         lcd_sendByte(LCD_DATA_REG,cChar);     
      }
      break;
   }
}


void lcd_printf(char *cStr)
{
   uint8_t ui8Cnt=0;
   
   for( ;cStr[ui8Cnt]!='\0'; ui8Cnt++ )
   {
      lcd_putC(cStr[ui8Cnt]);
   }
}
void lcd_PrintSP(char *cStr)
{
   uint8_t ui8Cnt=0;
   
   for( ;ui8Cnt < MAX_LCD_TXT_LEN; ui8Cnt++ )
   {
	   if(cStr[ui8Cnt]!='\0')
	   {
		   lcd_putC(cStr[ui8Cnt]);
	   }
	   else
	   {
		   lcd_putC(' ');
		   break;
	   }
   }
   for( ;ui8Cnt < MAX_LCD_TXT_LEN; ui8Cnt++ )
   {
	   lcd_putC(' ');
   }
}
void lcd_PrintSP_d(char *cStr,uint8_t len)
{
   uint8_t ui8Cnt=0;
   
   for( ;ui8Cnt < len; ui8Cnt++ )
   {
	   if(cStr[ui8Cnt]!='\0')
	   {
		   lcd_putC(cStr[ui8Cnt]);
	   }
	   else
	   {
		   lcd_putC(' ');
		   /*****bug fixed on 25-10-2018*******/
		   ui8Cnt++;
		   break;
	   }
   }
   for( ;ui8Cnt < len; ui8Cnt++ )
   {
	   lcd_putC(' ');
   }
}
/*
void lcd_writeCustomDesgn( char* cData, uint8_t ui8Loc)
{
   uint8_t ui8Cnt=0;
   
   ui8Loc = (ui8Loc*8)+64;
   lcd_sendByte(LCD_CTRL_REG,ui8Loc);
   for(ui8Cnt=0;ui8Cnt<8;ui8Cnt++)
   {
      lcd_sendByte(LCD_DATA_REG,cData[ui8Cnt]);
   }
}
*/
void lcd_showCustomDesgn(uint8_t num,uint8_t ui8X, uint8_t ui8Y)
{
lcd_goToXy(ui8X,ui8Y);	
lcd_sendByte(LCD_DATA_REG,num);
}
/*
void lcd_writeCustomDesgn( char* cData, int8 i8Loc) 
{
   int8 i8Count=0;
   
   i8Loc = (i8Loc*8)+64;
   lcd_send_byte(0,i8Loc);
   for(i8Count=0;i8Count<8;i8Count++)
   {
      lcd_send_byte(1,cData[i8Count]);
   }
   delay_ms(10);   
}
*/
/*
void lcd_clearLines( uint8_t ui8StrtCnt, uint8_t ui8EndCnt, uint8_t ui8LocX, uint8_t ui8LocY)
{
   uint8_t ui8Cnt=0;
   
   ui8Cnt = ui8StrtCnt;
   for(; ui8Cnt<=ui8EndCnt;ui8Cnt++)
   {
      lcd_goToXy(1,ui8Cnt);
      lcd_printf("                ");
   }
   lcd_goToXy(ui8LocX,ui8LocY);
}

void lcd_cursor_on(void)
{
//   lcd_sendByte(LCD_CTRL_REG,LCD_CURSOR_ON);
   lcd_sendByte(LCD_CTRL_REG,LCD_CRSR_BLINK);
}

void lcd_cursor_off(void)
{
   lcd_sendByte(LCD_CTRL_REG,LCD_CURSOR_OFF);
}

void lcd_blink_on(void)
{
	
}

void lcd_blink_off(void)
{
}
*/

void lcd_cursor_on(void)
{
   lcd_sendByte(LCD_CTRL_REG,LCD_CURSOR_ON);   
//   lcd_sendByte(LCD_CTRL_REG,LCD_CRSR_BLINK);
}

void lcd_cursor_off(void)
{
   lcd_sendByte(LCD_CTRL_REG,LCD_CURSOR_OFF);
}

void lcd_blink_on(void)
{
   lcd_sendByte(LCD_CTRL_REG,LCD_CRSR_BLINK);
}

void lcd_blink_off(void)
{
   lcd_sendByte(LCD_CTRL_REG,LCD_CURSOR_OFF);
}

void lcd_clearLines( uint8_t ui8StrtCnt, uint8_t ui8EndCnt, uint8_t ui8LocX, uint8_t ui8LocY)
{
   uint8_t ui8Cnt=0;
   
   ui8Cnt = ui8StrtCnt;
   for(; ui8Cnt<=ui8EndCnt;ui8Cnt++)
   {
      lcd_goToXy(1,ui8Cnt);
      lcd_printf("                ");
   }
   lcd_goToXy(ui8LocX,ui8LocY);
}

void lcd_clear_screen(void)
{
   lcd_sendByte(LCD_CTRL_REG,LCD_CLR_SCR);
}

void lcd_writeCustomDesgn( char* cData, uint8_t ui8Loc)
{
   uint8_t ui8Cnt=0;
   
   ui8Loc = (ui8Loc*8)+64;
   lcd_sendByte(LCD_CTRL_REG,ui8Loc);
   for(ui8Cnt=0;ui8Cnt<8;ui8Cnt++)
   {
      lcd_sendByte(LCD_DATA_REG,cData[ui8Cnt]);
   }
}
void lcd_printnum(uint8_t x,uint8_t y,uint16_t num,uint8_t print_len)
{
  char stri[10];  
  itoa(num, stri,10);
  lcd_update_hdlr(x,y,stri,print_len);
  //lcd_PrintSP_d(stri,print_len);
}
void vLCD_BackLight_ctl(G_CTL_CODES ctl)
{
	if(ON == ctl)
	{
		LCD_BL = !ctl;
	}
	else if(OFF == ctl)
	{
		LCD_BL = !ctl;
	}
}

uint8_t lcd_recieveNibble()
{
	uint8_t val=0;
	//uint8_t alp1 = 0x50;
	//uint8_t alp2 = 0x30;
	
	DelayUs(1);
	LCD_EN = 1;
	DelayUs(2);
	val = (P7<<2) & 0xF0;
	LCD_EN = 0;
	DelayUs(1);
	LCD_EN = 1;
	DelayUs(2);
	val = (val)|(((P7<<2) & 0xF0)>>4);
	LCD_EN = 0;
	DelayUs(2);
	return val;
}

/*
uint8_t lcd_read_char()
{
   
   uint8_t r=0;
   delayMs(1);
   LCD_RS = 0;
   LCD_RW = 0;
   DelayUs(1);
   LCD_EN = 0;
   lcd_sendNibble(8);
   lcd_sendNibble(6);
   LCD_RS = 1;
   LCD_RW = 1; 
   DelayUs(1);
   PM2= PM2 | 0xF0;
   LCD_EN = 0; 
   DelayUs(1);
   r=lcd_recieveNibble();
   PM2=	PM2 & 0x0F; 
   P2= P2 & 0x0F;
   return r;
}
*/
/*
void lcd_read_char(uint8_t line,char *str)
{
   
   uint8_t r=0;
   uint8_t i=0;
   if(line == 1)
   {
        lcd_goToXy(1,1);
   }
   else if(line == 2)
   {
	lcd_goToXy(1,2);   
   }
   else
   {
	   
   }
   LCD_RS = 1;
   LCD_RW = 1;//high
   PM2= PM2 | 0xF0;
   DelayUs(1);
   for(i=0;i<46;i++)
   {
	DelayUs(500);
	r=lcd_recieveNibble();
	*(str+i)=r;
	
   }
   *(str+i)='\0';
   PM2=	PM2 & 0x0F; 
   P2= P2 & 0x0F;
   LCD_RW = 0;//low
   
}
*/
void scrolltext()
{
	LCD_RW = 0;
    	lcd_sendByte(0,0x1C);
}

uint8_t lcd_read_char(uint8_t line,char *main_str,uint8_t main_str_len)
{
   uint8_t ret=SUCCESS;
   uint8_t r=0;
   uint8_t i=0;
   char str[20]={0};
   if(main_str_len > 16)
   {
	  main_str_len=16; 
   }
   if(line == 1)
   {
        lcd_goToXy(1,1);
   }
   else if(line == 2)
   {
	lcd_goToXy(1,2);   
   }
   else
   {
	   
   }
   LCD_RS = 1;
   LCD_RW = 1;//high
   PM7= PM7 | 0x3C;
   DelayUs(1);
   for(i=0;i<main_str_len;i++)
   {
	DelayUs(500);
	r=lcd_recieveNibble();
	*(str+i)=r;	 
	if(r != main_str[i])
	{
		ret = FAILURE;
		break;
	}
   }   
   
   PM7=	PM7 & 0xC3; 
   P7= P7 & 0xC3;
   LCD_RW = 0;//low
   return ret;
}
void lcd_hdlr(void)
{
	G_RESP_CODE ret = FAILURE;
	lcd_tick++;
	switch(lcd_state)
	{
		case 0:
		{
			/*if(lcd_update == 1)
			{
				lcd_update = 0;
				
				lcd_state = 2;
			}
			else
			{
				lcd_read_retry++;
				if(lcd_read_retry >= LCD_READ_INT)
				{
					lcd_read_retry = 0;	
					lcd_state = 2;
					sprintf((char*)debug_array,"LCD read test %d\r\n",lcd_tick);
					R_UART0_Send(debug_array,strlen((char*)debug_array));
				}			
			}*/
			lcd_read_retry++;
			if(lcd_read_retry >= LCD_READ_INT)
			{
				lcd_read_retry = 0;	
				lcd_state = 2;
				sprintf((char*)debug_array,"LCD read test %d\r\n",lcd_tick);
				//R_UART0_Send(debug_array,strlen((char*)debug_array));
			}
			break;
		}
		case 1:
		{
			lcd_goToXy(1,1);
			lcd_PrintSP_d(lcd_array[0],MAX_LCD_TXT_LEN);
			lcd_goToXy(1,2);
			lcd_PrintSP_d(lcd_array[1],MAX_LCD_TXT_LEN);
			lcd_state = 2;
			break;
		}
		case 2:
		{
			ret = lcd_read_char(1,lcd_array[0],16);
			if(ret == SUCCESS)
			{
				ret = lcd_read_char(2,lcd_array[1],16);
				if(ret == SUCCESS)
				{
					lcd_state = 0;
					lcd_read_retry = 0;
				}
			}
			if(ret == FAILURE)
			{
				lcd_read_retry++;
				if(lcd_read_retry >= 5)
				{
					lcd_read_retry = 0;
					lcd_state = 1;
					lcd_init();
    					lcd_clear_screen();
					lcd_write_retry++;
					if(lcd_write_retry >= 5)
					{
						lcd_write_retry = 0;
						lcd_state = 0;
						sprintf((char*)debug_array,"LCD write RETRY OVER %d\r\n",lcd_tick);
						//R_UART0_Send(debug_array,strlen((char*)debug_array));
					}
					else
					{
						sprintf((char*)debug_array,"LCD read RETRY OVER %d\r\n",lcd_tick);
						//R_UART0_Send(debug_array,strlen((char*)debug_array));
					}
				}
				else
				{
					sprintf((char*)debug_array,"LCD read RETRYing %d\r\n",lcd_tick);
					//R_UART0_Send(debug_array,strlen((char*)debug_array));
				}
			}
			else
			{
				sprintf((char*)debug_array,"LCD print success %d\r\n",lcd_tick);
				//R_UART0_Send(debug_array,strlen((char*)debug_array));
			}
			break;
		}
		default:
		break;
	}	
}
G_RESP_CODE lcd_update_hdlr(uint8_t x,uint8_t y,char *str,uint8_t len)
{
	uint8_t k = 0,i = 0,toggle = 0,j = 0;	 
	if(str != NULL)
	{
		if((x >= MAX_LCD_TXT_LEN) || (y >= 2) || (x > MAX_LCD_TXT_LEN))
		{
			return FAILURE;
		}
		k = MAX_LCD_TXT_LEN - x;
		k = (len > k)?k:len;
		for(i = 0;i<k;i++)
		{
			if(toggle == 0)
			{
				if(*str == '\0')
				{
					toggle  = 1;
					lcd_array[y][x] = ' ';
				}
				else
				{
					lcd_array[y][x] = *str;
					str++;
				}
			}
			else
			{
				lcd_array[y][x] = ' ';
			}
			x++;
		}
		//x = x -i;
		if(y == 0)
		{
			lcd_goToXy(1,1);
			lcd_PrintSP_d(lcd_array[0],16);
		}
		if(y == 1)
		{
			lcd_goToXy(1,2);
			lcd_PrintSP_d(lcd_array[1],16);
		}
	}
	else
	{
		lcd_goToXy(1,1);
		lcd_PrintSP_d(lcd_array[0],16);
		lcd_goToXy(1,2);
		lcd_PrintSP_d(lcd_array[1],16);
	}
	///lcd_update = 1;
	lcd_state = 2;
	/**clearing lcd_read_retry is mandatory.refer lcd_state 0**/
	lcd_read_retry = 0;
	lcd_write_retry = 0;
	
	
	
}
 