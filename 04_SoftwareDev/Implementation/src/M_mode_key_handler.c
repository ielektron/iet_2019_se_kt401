#include "A_main_app_handler.h"
#include "M_mode_key_handler.h"
#include "M_ZONE_intr_hdlr.h"
#include "M_led_handler.h"
#include "M_eeprom_handler.h"
#include "A_UI_handler.h"
#include "M_led_handler.h" 
#include "PIN_CFG.h"


//debounce time
#define MAX_KEY_DB_TIME 2

#define NO_UPDATE 0
#define UPDATE 1



uint8_t u8_G_pres_state = DEFAULT_MODE;
uint8_t u8_G_prev_state=DEFAULT_MODE;
uint8_t u8_G_state_change = 0;
uint8_t u8_dbcnt = 0;

MODE_KEY_TYPE u8_G_key_type = EXT_KEY;
SYS_MODE_E u8_G_sys_mode = DEFAULT_MODE;
SYS_MODE_E u8_G_prev_mode = DEFAULT_MODE;
 
SYS_MODE_E m_vMKEY_monitor(void)
{
	SYS_MODE_E ret = DEFAULT_MODE;	 
	u8_G_pres_state=MKEY1_IN;
	u8_G_pres_state=(u8_G_pres_state<<1)|MKEY2_IN;
	if(u8_G_prev_state!=u8_G_pres_state)
	{
		u8_G_prev_state=u8_G_pres_state;
		u8_dbcnt=0;
		u8_G_state_change=1;			
	}
	if(u8_G_state_change == 1)
	{
		u8_dbcnt++;
		if(u8_dbcnt > MAX_KEY_DB_TIME)
		{
			if(u8_G_pres_state-1 != u8_G_sys_mode)
			{
				u8_G_sys_mode=u8_G_pres_state-1;
			}				
			u8_G_state_change = 0;
			u8_dbcnt = 0;				 			
		}
	}
	 
	if(u8_G_prev_mode - u8_G_sys_mode)
	{		 
		u8_G_prev_mode = u8_G_sys_mode;
		ret = u8_G_sys_mode;
		if((u8_G_sys_mode == ARM_MODE)||( u8_G_sys_mode == PART_ARM_MODE))
		{
			m_u8LED_ctrl_led(LED_ARM,ON);			 
		}
		else if(u8_G_sys_mode == DISARM_MODE)
		{
			m_u8LED_ctrl_led(LED_ARM,OFF);			 
		}
		E2P_LOG_STORE();
	}
	return ret;
}

G_RESP_CODE m_u8ModeHdlr_update(const uint8_t *dat) 
{
	G_RESP_CODE ret = SUCCESS;
	 
	sys_data.u8_G_system_mode = *(dat + CURRENT_SYS_MODE_START_INDEX);
	return ret;
} 


SYS_MODE_E m_u8MKEY_sts_get(void)
{ 
	return u8_G_sys_mode;
}
void m_u8MKEY_sts_set(SYS_MODE_E mode)
{ 
	u8_G_sys_mode = mode;
}
MODE_KEY_TYPE m_u8MKEY_type_get(void)
{ 
	return u8_G_key_type;
}
