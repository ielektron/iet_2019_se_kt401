#ifndef M_RTC_HANDLER_H
#define M_RTC_HANDLER_H


#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"


#define RTC_TIME_LEN 30

#define RTC_RETRY_ENABLE 0

G_RESP_CODE RTC_update(void);
uint8_t* RTC_LogStore(uint8_t *arr);
G_RESP_CODE RTC_currTimeToArray(uint8_t *arr);

#ifdef RTC_RETRY_ENABLE
void RTC_retryHandler(void);
void RTC_retryManager(void);
#endif
//G_RESP_CODE m_u8RTC_strTime_set(uint8_t *current_time);
//G_RESP_CODE m_u8RTC_test(G_CTL_CODES ctl);
//G_RESP_CODE m_u8RTC_time_get(RTC_DATE_TIME *rtc_cur_dat);
//void m_u8RTC_ref_set(uint8_t *buff);
//void m_u8RTC_ref_get(RTC_DATE_TIME *rtc_cur_dat);
#endif
