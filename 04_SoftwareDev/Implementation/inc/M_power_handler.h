#ifndef M_POWER_HANDLER_H
#define M_POWER_HANDLER_H

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"

typedef enum
{
	POW_AC = 0,
	BAT_NORM,
	BAT_LOW,
	BAT_SHUT,
	BAT_CONN,
	BAT_DISCONN,
	ES_DEFAULT
}POW_MODE_T;

void m_POW_monitor(void);
void m_POW_reset(void);
#endif
