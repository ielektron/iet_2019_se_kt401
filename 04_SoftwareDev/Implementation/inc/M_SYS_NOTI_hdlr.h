#ifndef M_SYS_NOTI_HDLR_H
#define M_SYS_NOTI_HDLR_H

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "A_main_app_handler.h"

typedef enum
{
	REL_SOUNDER = 0,
	REL_DIALER,
}RELAY_TYPE_T;


void M_NOTI_RELAYCtl(RELAY_TYPE_T type,G_CTL_CODES ctl);
G_RESP_CODE NOTI_hdlr(E_SYS_EVENTS event,uint8_t status);

#endif