#ifndef H_M66_GSM_DRIVER_H
#define H_M66_GSM_DRIVER_H

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"


 
typedef char CIRC_BUFF_TYPE;

typedef enum
{
  GSM_BUFF_Q = 0
}GSM_Q_NAME;

typedef G_RESP_CODE(*AT_CALLBACK)(uint8_t param);

G_RESP_CODE h_u8GSM_call_send(uint8_t *phone_no);
G_RESP_CODE h_u8GSM_call_hang(void);
G_RESP_CODE h_u8GSM_sms_send(uint8_t *phone_no,const uint8_t *data);

void h_u8GSM_time_get(char *time_arr);
G_RESP_CODE h_u8GSM_time_update(void);
G_RESP_CODE h_u8GSM_record_start(void);


void h_u8GSM_at_reset(void);
G_RESP_CODE h_u8GSM_record_del(void);
G_RESP_CODE h_u8GSM_record_start(void);
G_RESP_CODE h_u8GSM_record_stop(void);
G_RESP_CODE h_u8GSM_init(void);
G_RESP_CODE h_u8GSM_SelfTest(void);
G_RESP_CODE h_u8GSM_call_resp_hdlr(void);
G_RESP_CODE sms_resp_handler(uint8_t param);
void h_u8GSM_CIRCflush(void);

G_RESP_CODE h_u8GSM_uart_test(void);
G_RESP_CODE h_u8GSM_ctl(G_CTL_CODES ctl);
G_RESP_CODE h_u8GSM_PwrKeyCtl(G_CTL_CODES ctl);
G_RESP_CODE h_u8GSM_CallPlayBack(void); 
void h_u8GSM_PowSeq(void);
void h_u8GSM_AT_TIMEOUT_CbRegister(AT_CALLBACK gsm_cb);
G_RESP_CODE h_u8GSM_Q_send(GSM_Q_NAME qname,CIRC_BUFF_TYPE *val);
G_RESP_CODE h_u8GSM_Q_chk(GSM_Q_NAME qname);
#endif