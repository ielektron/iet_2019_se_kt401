#ifndef M_GSM_INTERFACE_HDLR_H
#define M_GSM_INTERFACE_HDLR_H

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
 


#define MAX_GSM_RESP_INT 10
#define MAX_CIRC_BUFF_DEPTH 255




extern uint8_t TEST[100],test_cnt;
typedef char CIRC_BUFF_TYPE;
typedef enum
{
	GSM_LINE_NO_HEAD = 0,
	GSM_LINE_HEAD_L = 1,
	GSM_LINE_HEAD_H = 2,
	GSM_LINE_TAIL_L = 3,
	GSM_LINE_TAIL_H = 4,
	GSM_IDLE=5,
}GSM_LINE_TYPE;

typedef enum
{
	GSM_DEFAULT = 0,
	GSM_OK,
	GSM_NOT_OK,
	GSM_NO_SIM,
	GSM_STS_END
}GSM_STS_T;

typedef struct 
{
    CIRC_BUFF_TYPE *const buffer;
    uint8_t head;
    uint8_t tail;
    const uint8_t maxlen;
}CIRC_BUFF_ST;



typedef enum
{
	GSM_QUEUE=0, 
}GSM_Q_NAME;

G_RESP_CODE h_u8GSM_Q_send(GSM_Q_NAME qname,CIRC_BUFF_TYPE *val);
void GSM_CommandSend(E_SYS_EVENTS cmd,uint8_t resp);
void GSM_CommandSend_Ext(E_SYS_EVENTS cmd,uint8_t resp,uint8_t *data,uint8_t len);
GSM_STS_T u8get_GSM_sts(void);
void GSM_interface_hdlr(void);
G_RESP_CODE h_u8GSM_PwrKeyCtl(G_CTL_CODES ctl);
G_RESP_CODE h_u8GSM_ctl(G_CTL_CODES ctl);

#endif