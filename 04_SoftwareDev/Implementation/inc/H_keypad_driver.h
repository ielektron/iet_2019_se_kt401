#ifndef KEYPAD_H
#define KEYPAD_H

#define KEY_ROW0 P5.0
#define KEY_ROW1 P5.1
#define KEY_ROW2 P5.2
#define KEY_ROW3 P5.3

#define KEY_COL0 P5.4
#define KEY_COL1 P5.5
#define KEY_COL2 P1.7

typedef enum
{
	//KEY must be added as preficx for identification
	MENU ='5' , 
	ENTER ='#',
	BACK ='*',
 	UP ='2',
 	DOWN ='8',
 	LEFT ='4',
	RIGHT ='6',
	//TOGGLE ='6',
 	KEY_RESET ='0',
 	DIA_KEY ='3',
	MUTE_KEY ='0',
	RECORD ='4'
}KEY_TYPE_T;
 
#define PRESSED 1 
#define RELEASED 0
#define MAX_KEY_DB_TIME 2

uint8_t keypad_read(void);
void keypad_Handler(void); 
void get_key(KEY_TYPE_T *key);
KEY_TYPE_T KEY_rawGet(void);
KEY_TYPE_T KEY_stsGet(void);

#endif