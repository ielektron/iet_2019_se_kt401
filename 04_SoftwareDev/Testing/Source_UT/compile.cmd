set mypath=%cd%
for %%I in (.) do set ProjName=%%~nxI
set ProjName=gtst_M_ADC_hdlr
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_main.o" "%mypath%\TST\gtst_main.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_M_ADC_hdlr.o" "%mypath%\TST\gtst_M_ADC_hdlr.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\GTST\gtest\gtest-all.o" "%mypath%\GTST\gtest\gtest-all.cc"
g++ -ftest-coverage -fprofile-arcs -o %ProjName% "DBG\GTST\gtest\gtest-all.o"  "DBG\TST\gtst_main.o" "DBG\TST\gtst_M_ADC_hdlr.o" -lpthread
%ProjName%.exe --gtest_output=xml:RPT\RESULTS\%ProjName%_results.xml > TST\gtst_M_ADC_hdlr_result.txt
gcov -o "%mypath%\DBG\TST\gtst_main.gcno" "%mypath%\TST\gtst_main.cpp"
gcov -o "%mypath%\DBG\TST\gtst_M_ADC_hdlr.gcno" "%mypath%\TST\gtst_M_ADC_hdlr.cpp"
echo "test_completed:gtst_M_ADC_hdlr"
set mypath=%cd%
for %%I in (.) do set ProjName=%%~nxI
set ProjName=gtst_M_led_handler
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_main.o" "%mypath%\TST\gtst_main.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_M_led_handler.o" "%mypath%\TST\gtst_M_led_handler.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\GTST\gtest\gtest-all.o" "%mypath%\GTST\gtest\gtest-all.cc"
g++ -ftest-coverage -fprofile-arcs -o %ProjName% "DBG\GTST\gtest\gtest-all.o"  "DBG\TST\gtst_main.o" "DBG\TST\gtst_M_led_handler.o" -lpthread
%ProjName%.exe --gtest_output=xml:RPT\RESULTS\%ProjName%_results.xml > TST\gtst_M_led_handler_result.txt
gcov -o "%mypath%\DBG\TST\gtst_main.gcno" "%mypath%\TST\gtst_main.cpp"
gcov -o "%mypath%\DBG\TST\gtst_M_led_handler.gcno" "%mypath%\TST\gtst_M_led_handler.cpp"
echo "test_completed:gtst_M_led_handler"
set mypath=%cd%
for %%I in (.) do set ProjName=%%~nxI
set ProjName=gtst_M_mode_key_handler
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_main.o" "%mypath%\TST\gtst_main.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_M_mode_key_handler.o" "%mypath%\TST\gtst_M_mode_key_handler.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\GTST\gtest\gtest-all.o" "%mypath%\GTST\gtest\gtest-all.cc"
g++ -ftest-coverage -fprofile-arcs -o %ProjName% "DBG\GTST\gtest\gtest-all.o"  "DBG\TST\gtst_main.o" "DBG\TST\gtst_M_mode_key_handler.o" -lpthread
%ProjName%.exe --gtest_output=xml:RPT\RESULTS\%ProjName%_results.xml > TST\gtst_M_mode_key_handler_result.txt
gcov -o "%mypath%\DBG\TST\gtst_main.gcno" "%mypath%\TST\gtst_main.cpp"
gcov -o "%mypath%\DBG\TST\gtst_M_mode_key_handler.gcno" "%mypath%\TST\gtst_M_mode_key_handler.cpp"
echo "test_completed:gtst_M_mode_key_handler"
set mypath=%cd%
for %%I in (.) do set ProjName=%%~nxI
set ProjName=gtst_M_power_handler
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_main.o" "%mypath%\TST\gtst_main.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_M_power_handler.o" "%mypath%\TST\gtst_M_power_handler.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\GTST\gtest\gtest-all.o" "%mypath%\GTST\gtest\gtest-all.cc"
g++ -ftest-coverage -fprofile-arcs -o %ProjName% "DBG\GTST\gtest\gtest-all.o"  "DBG\TST\gtst_main.o" "DBG\TST\gtst_M_power_handler.o" -lpthread
%ProjName%.exe --gtest_output=xml:RPT\RESULTS\%ProjName%_results.xml > TST\gtst_M_power_handler_result.txt
gcov -o "%mypath%\DBG\TST\gtst_main.gcno" "%mypath%\TST\gtst_main.cpp"
gcov -o "%mypath%\DBG\TST\gtst_M_power_handler.gcno" "%mypath%\TST\gtst_M_power_handler.cpp"
echo "test_completed:gtst_M_power_handler"
set mypath=%cd%
for %%I in (.) do set ProjName=%%~nxI
set ProjName=gtst_M_rtc_handler
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_main.o" "%mypath%\TST\gtst_main.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_M_rtc_handler.o" "%mypath%\TST\gtst_M_rtc_handler.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\GTST\gtest\gtest-all.o" "%mypath%\GTST\gtest\gtest-all.cc"
g++ -ftest-coverage -fprofile-arcs -o %ProjName% "DBG\GTST\gtest\gtest-all.o"  "DBG\TST\gtst_main.o" "DBG\TST\gtst_M_rtc_handler.o" -lpthread
%ProjName%.exe --gtest_output=xml:RPT\RESULTS\%ProjName%_results.xml > TST\gtst_M_rtc_handler_result.txt
gcov -o "%mypath%\DBG\TST\gtst_main.gcno" "%mypath%\TST\gtst_main.cpp"
gcov -o "%mypath%\DBG\TST\gtst_M_rtc_handler.gcno" "%mypath%\TST\gtst_M_rtc_handler.cpp"
echo "test_completed:gtst_M_rtc_handler"
set mypath=%cd%
for %%I in (.) do set ProjName=%%~nxI
set ProjName=gtst_M_SYS_NOTI_hdlr
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_main.o" "%mypath%\TST\gtst_main.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_M_SYS_NOTI_hdlr.o" "%mypath%\TST\gtst_M_SYS_NOTI_hdlr.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\GTST\gtest\gtest-all.o" "%mypath%\GTST\gtest\gtest-all.cc"
g++ -ftest-coverage -fprofile-arcs -o %ProjName% "DBG\GTST\gtest\gtest-all.o"  "DBG\TST\gtst_main.o" "DBG\TST\gtst_M_SYS_NOTI_hdlr.o" -lpthread
%ProjName%.exe --gtest_output=xml:RPT\RESULTS\%ProjName%_results.xml > TST\gtst_M_SYS_NOTI_hdlr_result.txt
gcov -o "%mypath%\DBG\TST\gtst_main.gcno" "%mypath%\TST\gtst_main.cpp"
gcov -o "%mypath%\DBG\TST\gtst_M_SYS_NOTI_hdlr.gcno" "%mypath%\TST\gtst_M_SYS_NOTI_hdlr.cpp"
echo "test_completed:gtst_M_SYS_NOTI_hdlr"
gcovr -r "%mypath%" > cover.txt
cd "%mypath%\RPT\COVERAGE"
gcovr -r "%mypath%" --html --html-details -o coverage.html
cd "%mypath%"
echo "static code analysis starts";
cppcheck INC\ 2> analysis.txt
python test_results.py
echo "all_test_completed"
pause
