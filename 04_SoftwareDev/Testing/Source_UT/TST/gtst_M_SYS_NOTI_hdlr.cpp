
/*

filename : gtst_sys_noti_handler.cpp
author : rudresh
created at : 2019-04-06 04:57:55.968330

*/
    
# include "gtest/gtest.h"
 
 

#include "r_cg_userdefine.h"
#include "r_cg_macrodriver.h"
#include "M_power_handler.h"
extern "C"
{
#include "M_SYS_NOTI_hdlr.h"
#include "M_SYS_NOTI_hdlr.c"
#include "SysNotIHandlerStub.h"
#include "SysNotIHandlerStub.c"

}


//RequirementTest 
TEST(NOTI_hdlrTest, RequirementTest1) 
{
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_ALT_STOP, BAT_NORM));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, OFF);
	ASSERT_EQ(a.LED_ALERT, OFF);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest2)
{
	init();
	setter(OFF);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_ALT_TRIG, BAT_NORM));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, NILL);
	ASSERT_EQ(a.LED_BAD, NILL);
	ASSERT_EQ(a.LED_AC, NILL);
	ASSERT_EQ(a.LED_ALERT, ON);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest3)
{
	init();
	setter(ON);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_ALT_TRIG, BAT_NORM));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, ON);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, NILL);
	ASSERT_EQ(a.LED_BAD, NILL);
	ASSERT_EQ(a.LED_AC, NILL);
	ASSERT_EQ(a.LED_ALERT, ON);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest4)
{
	init();
	setter(ON);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_POWER, BAT_NORM));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, ON);
	ASSERT_EQ(a.LED_BAD, OFF);
	ASSERT_EQ(a.LED_AC, OFF);
	ASSERT_EQ(a.LED_ALERT, NILL);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest5)
{
	init();
	setter(ON);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_POWER, BAT_LOW));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, OFF);
	ASSERT_EQ(a.LED_BAD, ON);
	ASSERT_EQ(a.LED_AC, OFF);
	ASSERT_EQ(a.LED_ALERT, NILL);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest6)
{
	init();
	setter(ON);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_POWER, BAT_SHUT));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, OFF);
	ASSERT_EQ(a.LED_BAD, ON);
	ASSERT_EQ(a.LED_AC, OFF);
	ASSERT_EQ(a.LED_ALERT, NILL);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest7)
{
	init();
	setter(ON);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_POWER, POW_AC));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, NILL);
	ASSERT_EQ(a.LED_BAD, NILL);
	ASSERT_EQ(a.LED_AC, ON);
	ASSERT_EQ(a.LED_ALERT, NILL);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest8)
{
	init(); 
	setter(ON);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_POWER, BAT_DISCONN));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, OFF);
	ASSERT_EQ(a.LED_BAD, OFF);
	ASSERT_EQ(a.LED_AC, NILL);
	ASSERT_EQ(a.LED_ALERT, NILL);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest9)
{
	init();
	setter(ON);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_POWER, BAT_CONN));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, ON);
	ASSERT_EQ(a.LED_BAD, OFF);
	ASSERT_EQ(a.LED_AC, NILL);
	ASSERT_EQ(a.LED_ALERT, NILL);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest10)
{
	init();
	setter(ON);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_POWER, BAT_DISCONN));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, OFF);
	ASSERT_EQ(a.LED_BAD, OFF);
	ASSERT_EQ(a.LED_AC, NILL);
	ASSERT_EQ(a.LED_ALERT, NILL);
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_POWER, BAT_CONN));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, ON);
	ASSERT_EQ(a.LED_BAD, OFF);
	ASSERT_EQ(a.LED_AC, NILL);
	ASSERT_EQ(a.LED_ALERT, NILL);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest11)
{
	init();
	setter(OFF);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_DEFAULT, ES_DEFAULT));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, NILL);
	ASSERT_EQ(a.LED_BAD, NILL);
	ASSERT_EQ(a.LED_AC, NILL);
	ASSERT_EQ(a.LED_ALERT, NILL);
}

//RequirementTest

TEST(NOTI_hdlrTest, RequirementTest12)
{
	init();
	setter(OFF);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_POWER, ES_DEFAULT));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, NILL);
	ASSERT_EQ(a.LED_BAD, NILL);
	ASSERT_EQ(a.LED_AC, NILL);
	ASSERT_EQ(a.LED_ALERT, NILL);
}

//LogicalBoundrayTest

TEST(NOTI_hdlrTest, LogicalBoundaryTest)
{
	init();
	setter(OFF);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_DEFAULT, POW_AC));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, NILL);
	ASSERT_EQ(a.LED_BAD, NILL);
	ASSERT_EQ(a.LED_AC, NILL);
	ASSERT_EQ(a.LED_ALERT, NILL);
}

//LogicalBoundaryTest

TEST(NOTI_hdlrTest, LogicalBoundaryTest1)
{
	init();
	setter(OFF);
	RELAYLED a;
	ASSERT_EQ(SUCCESS, NOTI_hdlr(E_END, ES_DEFAULT));
	GetStatus(&a);
	ASSERT_EQ(a.RELAY_S, NILL);
	ASSERT_EQ(a.RELAY_E, NILL);
	ASSERT_EQ(a.LED_GOOD, NILL);
	ASSERT_EQ(a.LED_BAD, NILL);
	ASSERT_EQ(a.LED_AC, NILL);
	ASSERT_EQ(a.LED_ALERT, NILL);
}