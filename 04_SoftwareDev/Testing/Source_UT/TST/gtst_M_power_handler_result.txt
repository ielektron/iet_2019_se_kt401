Running main() from gtest_main.cc
[==========] Running 15 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 15 tests from POWER_HANDLER_TEST
[ RUN      ] POWER_HANDLER_TEST.RequirementTest1
[       OK ] POWER_HANDLER_TEST.RequirementTest1 (0 ms)
[ RUN      ] POWER_HANDLER_TEST.RequirementTest2
[       OK ] POWER_HANDLER_TEST.RequirementTest2 (0 ms)
[ RUN      ] POWER_HANDLER_TEST.RequirementTest3
[       OK ] POWER_HANDLER_TEST.RequirementTest3 (0 ms)
[ RUN      ] POWER_HANDLER_TEST.BatShutToBatNormTest
[       OK ] POWER_HANDLER_TEST.BatShutToBatNormTest (0 ms)
[ RUN      ] POWER_HANDLER_TEST.BatLowToBatNormTest
[       OK ] POWER_HANDLER_TEST.BatLowToBatNormTest (0 ms)
[ RUN      ] POWER_HANDLER_TEST.BatShutToBatLowTest
[       OK ] POWER_HANDLER_TEST.BatShutToBatLowTest (0 ms)
[ RUN      ] POWER_HANDLER_TEST.PowStsCheckWhenNoBattery_0
C:\Users\Admin\Desktop\Tiva\iet_2019_se_kt401\04_SoftwareDev\Testing\Source_UT\TST\gtst_M_power_handler.cpp:111: Failure
Expected equality of these values:
  POW_AC
    Which is: 0
  PowerSts
    Which is: 1
[  FAILED  ] POWER_HANDLER_TEST.PowStsCheckWhenNoBattery_0 (1 ms)
[ RUN      ] POWER_HANDLER_TEST.RequirementTest7
[       OK ] POWER_HANDLER_TEST.RequirementTest7 (0 ms)
[ RUN      ] POWER_HANDLER_TEST.RequirementTest8
[       OK ] POWER_HANDLER_TEST.RequirementTest8 (0 ms)
[ RUN      ] POWER_HANDLER_TEST.LogicalUpperBoundMaxTest
C:\Users\Admin\Desktop\Tiva\iet_2019_se_kt401\04_SoftwareDev\Testing\Source_UT\TST\gtst_M_power_handler.cpp:142: Failure
Expected equality of these values:
  ES_DEFAULT
    Which is: 6
  PowerSts
    Which is: 0
[  FAILED  ] POWER_HANDLER_TEST.LogicalUpperBoundMaxTest (0 ms)
[ RUN      ] POWER_HANDLER_TEST.LogicalUpperBoundMidTest
[       OK ] POWER_HANDLER_TEST.LogicalUpperBoundMidTest (0 ms)
[ RUN      ] POWER_HANDLER_TEST.LogicalUpperBoundMinTest
[       OK ] POWER_HANDLER_TEST.LogicalUpperBoundMinTest (0 ms)
[ RUN      ] POWER_HANDLER_TEST.LogicalLowerBoundMidTest
[       OK ] POWER_HANDLER_TEST.LogicalLowerBoundMidTest (0 ms)
[ RUN      ] POWER_HANDLER_TEST.LogicalLowerBoundMaxTest
[       OK ] POWER_HANDLER_TEST.LogicalLowerBoundMaxTest (0 ms)
[ RUN      ] POWER_HANDLER_TEST.PhysicalUpperBoundTest
[       OK ] POWER_HANDLER_TEST.PhysicalUpperBoundTest (0 ms)
[----------] 15 tests from POWER_HANDLER_TEST (2 ms total)

[----------] Global test environment tear-down
[==========] 15 tests from 1 test suite ran. (2 ms total)
[  PASSED  ] 13 tests.
[  FAILED  ] 2 tests, listed below:
[  FAILED  ] POWER_HANDLER_TEST.PowStsCheckWhenNoBattery_0
[  FAILED  ] POWER_HANDLER_TEST.LogicalUpperBoundMaxTest

 2 FAILED TESTS
