
/*

filename : gtst_main.cpp
author : rudresh
created at : 2019-04-06 04:57:55.952726

*/
    

#include <stdio.h>
#include "gtest/gtest.h"

GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from gtest_main.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
        