/***********************************************************************************************************************
Start
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/

#include "r_cg_userdefine.h"
#include "H_DS1307_rtc_driver.h"
//#include "M_GeneralLib.h"
//#include "M_GeneralLib.c"

 /***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/ 

 /***********************************************************************************************************************
Global variables and functions
**********************************************************************************************************************/

uint8_t year, month, day, hour, min, sec;
G_RESP_CODE result;

/***********************************************************************************************************************
* Function Name: getstatus
* Description  : This function stores the status of event.
* Arguments    : a - pointer to store the status of event
* Return Value : None
***********************************************************************************************************************/

void getstatus(G_RESP_CODE * a)
{
	*a = result;
}

/***********************************************************************************************************************
* Function Name: setter
* Description  : This function assign the global variables with the arguements passed in function.
* Arguments    : arg_year - year to be set 
				 arg_month - month to be set  
				 arg_day - day to be set
				 arg_hour - hour to be set  
				 arg_min - minutes to be set 
				 arg_sec - second to be set
* Return Value : None
***********************************************************************************************************************/

void setter(uint8_t arg_year, uint8_t arg_month, uint8_t arg_day, uint8_t arg_hour, uint8_t arg_min, uint8_t arg_sec)
{
	year = arg_year;
	month = arg_month;
	day = arg_day;
	hour = arg_hour;
	min = arg_min;
	sec = arg_sec;
}

/***********************************************************************************************************************
* Function Name: h_u8RTC_time_set
* Description  : This function sets rtc time.
* Arguments    : rtc_cur_dat - time to set 
* Return Value : None
***********************************************************************************************************************/

void h_u8RTC_time_set(RTC_DATE_TIME rtc_cur_dat)
{
	year = rtc_cur_dat.u8Year;
	month = rtc_cur_dat.u8Month;
	day = rtc_cur_dat.u8Day;
	hour = rtc_cur_dat.u8Hour;
	min = rtc_cur_dat.u8Minute;
	sec = rtc_cur_dat.u8Second;
}

/***********************************************************************************************************************
* Function Name: h_u8RTC_time_get
* Description  : This function get rtc time.
* Arguments    : rtc_cur_dat - to store rtc time 
* Return Value : success - I2C success
				 failure - I2C failure
***********************************************************************************************************************/

//c_cur_dat = 18 / 05 / 31, 18:48 : 57
uint8_t h_u8RTC_time_get(RTC_DATE_TIME *rtc_cur_dat)
{

	rtc_cur_dat->u8Year = year;
	rtc_cur_dat-> u8Month=month;
	rtc_cur_dat->u8Day = day;
	rtc_cur_dat->u8Hour = hour;
	rtc_cur_dat->u8Minute = min;
	rtc_cur_dat->u8Second = sec;

	return SUCCESS;
}

/***********************************************************************************************************************
* Function Name: E2P_eventManager
* Description  : This function notifices the current events to eeprom .
* Arguments    : event - event id
				 event - status
* Return Value : None
***********************************************************************************************************************/

void E2P_eventManager(E_SYS_EVENTS event, uint8_t sts)
{
	result =(G_RESP_CODE) sts;
}
