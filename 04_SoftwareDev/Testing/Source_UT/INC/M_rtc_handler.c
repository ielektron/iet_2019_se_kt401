/***********************************************************************************************************************
Start
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/

#include "M_rtc_handler.h" 
#include "H_DS1307_rtc_driver.h"
#include "M_eeprom_handler.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "M_GeneralLib.h" 
#include "M_event_handler.h"

 /***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/ 

#define RTC_RETRY_FLAG 0x00
#define MAX_RTC_RETRY_CNT 5
 
 /***********************************************************************************************************************
Global variables and functions
**********************************************************************************************************************/

RTC_DATE_TIME RTC_currTime;

static uint16_t RTC_eventFlag = 0x00;
uint8_t RTC_retryCount = 0;

/***********************************************************************************************************************
* Function Name: RTC_get_str
* Description  : This function converts the rtc current time structure to string.
* Arguments    : arr - array to store the rtc current time structure data member contents
* Return Value : return any one value of the enum G_RESP_CODE
***********************************************************************************************************************/

G_RESP_CODE RTC_get_str(uint8_t *str);

/***********************************************************************************************************************
* Function Name: RTC_EventFlagManager
* Description  : This function stores the rtc event 
* Arguments    : flag_pos - event id
				 operation - event status
* Return Value : event status
***********************************************************************************************************************/

uint8_t RTC_EventFlagManager(uint8_t flag_pos,uint8_t operation);

/***********************************************************************************************************************
Extern variables
***********************************************************************************************************************/

/***********************************************************************************************************************
* Function Name: RTC_update
* Description  : This function updates the current system time.
* Arguments    : None
* Return Value : any one value of enum G_RESP_CODE
***********************************************************************************************************************/

G_RESP_CODE RTC_update(void)
{
	G_RESP_CODE ret = SUCCESS;
	RTC_DATE_TIME rtc_cur_dat;	
	
	//date_time_arr - 18/05/31,18:48:57 SIZE 18 BYTES
	//sprintf(str,"%d/%d/%d,%d:%d:%d",rtc_cur_dat.u8Year,rtc_cur_dat.u8Month,rtc_cur_dat.u8Day,
	//rtc_cur_dat.u8Hour,rtc_cur_dat.u8Minute,rtc_cur_dat.u8Second);
	
	ret =(G_RESP_CODE)h_u8RTC_time_get(&rtc_cur_dat);
	if(ret == SUCCESS)
	{
		if((rtc_cur_dat.u8Year >= 18)&&(rtc_cur_dat.u8Month<=12)&&(rtc_cur_dat.u8Day <=31)&&
			(rtc_cur_dat.u8Hour<=24)&&(rtc_cur_dat.u8Minute<=60)&&(rtc_cur_dat.u8Month!=0)&&(rtc_cur_dat.u8Day!=0))				
		{
			RTC_currTime = rtc_cur_dat;
			ret = SUCCESS;
		}
		else
		{
			ret = FAILURE;
		}
	}
	else
	{
		rtc_cur_dat.u8Hour = 0xFF;
	}
	return ret;
}

/***********************************************************************************************************************
* Function Name: RTC_LogStore
* Description  : This function updates the passed input array with current rtc time.
* Arguments    : arr - array to store the rtc time
* Return Value : return the pointer which points the end of the copied content in the array passed as arguement
***********************************************************************************************************************/

uint8_t* RTC_LogStore(uint8_t *arr)
{
	G_RESP_CODE ret =SUCCESS;
	uint8_t k = 0;
	uint8_t loc_date_time[RTC_TIME_LEN]={0};
	
	ret = (G_RESP_CODE)RTC_get_str(loc_date_time);
	if(ret == SUCCESS)
	{
		k =  u8GL_ncopy(arr,(uint8_t *)loc_date_time,RTC_TIME_LEN);
		arr = arr + k;		 
	}
	else
	{
		arr = NULL;
	}
	return arr;
}

/***********************************************************************************************************************
* Function Name: RTC_get_str
* Description  : This function converts the rtc current time structure to string.
* Arguments    : arr - array to store the rtc current time structure data member contents
* Return Value : return any one value of the enum G_RESP_CODE
***********************************************************************************************************************/

G_RESP_CODE RTC_get_str(uint8_t *str)
{
	G_RESP_CODE ret =SUCCESS;	 
	uint8_t num_arr[5] = {0};
	uint8_t k = 0;	 
	if((RTC_currTime.u8Year >= 18)&&(RTC_currTime.u8Month<=12)&&(RTC_currTime.u8Day <=31)&&
		(RTC_currTime.u8Hour<=24)&&(RTC_currTime.u8Minute<=60)&&(RTC_currTime.u8Month!=0)&&(RTC_currTime.u8Day!=0))				
	{		
		u8GL_num2str(RTC_currTime.u8Year,(char*)num_arr);
		k =  u8GL_ncopy(str,num_arr,2);
		str = str + k;
	       *str = '/';
	        str++;
		u8GL_num2str(RTC_currTime.u8Month,(char*)num_arr); 
		k =  u8GL_ncopy(str,num_arr,2);	
		str = str + k;
	       *str = '/';
	        str++;
		u8GL_num2str(RTC_currTime.u8Day,(char*)num_arr); 
		k =  u8GL_ncopy(str,num_arr,2);	
		str = str + k;
	       *str = ',';
	        str++;
		u8GL_num2str(RTC_currTime.u8Hour,(char*)num_arr); 
		k =  u8GL_ncopy(str,num_arr,2);	
		str = str + k;
	       *str = ':';
	        str++;
		u8GL_num2str(RTC_currTime.u8Minute,(char*)num_arr);
		k =  u8GL_ncopy(str,num_arr,2);
		str = str + k;
	       *str = ':';
	        str++;
		u8GL_num2str(RTC_currTime.u8Second,(char*)num_arr);
		k =  u8GL_ncopy(str,num_arr,2);
		str = str + k;      
	       *str = '\0';
	       ret = SUCCESS;	
	}
	else
	{
		ret = FAILURE;
	}	 
       return ret;
}


#ifdef RTC_RETRY_ENABLE

/***********************************************************************************************************************
* Function Name: RTC_retryHandler
* Description  : This function handles the retry mechanism for rtc.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/

void RTC_retryHandler(void)
{
	G_RESP_CODE ret = SUCCESS;
	if(RTC_eventFlag != 0)
	{
		if(RTC_eventFlag & 0x01)
		{
			ret = RTC_update();
			if(ret == SUCCESS)
			{
				RTC_EventFlagManager(RTC_RETRY_FLAG,FLAG_CLR);
				E2P_eventManager(E_RTC_RETRY_STS,SUCCESS);
				RTC_retryCount = 0;
			}
			else
			{
				RTC_retryCount++;
				if(RTC_retryCount > MAX_RTC_RETRY_CNT)
				{
					RTC_retryCount = 0;
					RTC_EventFlagManager(RTC_RETRY_FLAG,FLAG_CLR);
					E2P_eventManager(E_RTC_RETRY_STS,FAILURE);
				}
			}
		}
	}
}

/***********************************************************************************************************************
* Function Name: RTC_retryManager
* Description  : This function signals the rtc module to trigger the retry mechanism.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void RTC_retryManager(void)
{	 
	RTC_EventFlagManager(RTC_RETRY_FLAG,FLAG_SET);
	RTC_retryCount = 0;
}
#endif

/***********************************************************************************************************************
* Function Name: RTC_currTimeToArray
* Description  : This function converts the rtc current time structure to array.
* Arguments    : arr - array to store the rtc current time structure data member contents
* Return Value : return any one value of the enum G_RESP_CODE
***********************************************************************************************************************/

G_RESP_CODE RTC_currTimeToArray(uint8_t *arr)
{
	G_RESP_CODE ret = SUCCESS;
	*(arr + REF_RTC_YEAR) = RTC_currTime.u8Year;
	*(arr + REF_RTC_MONTH) = RTC_currTime.u8Month;
	*(arr + REF_RTC_DAY) = RTC_currTime.u8Day;
	*(arr + REF_RTC_HOUR) = RTC_currTime.u8Hour;
	*(arr + REF_RTC_MINUTE) = RTC_currTime.u8Minute;	
	return ret;
}

/***********************************************************************************************************************
* Function Name: RTC_EventFlagManager
* Description  : This function stores the rtc event 
* Arguments    : flag_pos - event id
				 operation - event status
* Return Value : event status
***********************************************************************************************************************/

uint8_t RTC_EventFlagManager(uint8_t flag_pos,uint8_t operation)
{
	uint8_t ret = 5;
	uint16_t chk_val = 0;
	switch(operation)
	{
	  	case FLAG_SET:
		{
			SETB(RTC_eventFlag,flag_pos);
			break;
		}
		case FLAG_CLR:
		{
			CLRB(RTC_eventFlag,flag_pos);
			break;
		}
		case FLAG_CHK:
		{
			chk_val = CHKB(RTC_eventFlag,flag_pos);
			if(chk_val)
			{
				ret = FLAG_SET;	
			}
			else
			{
				ret = FLAG_CLR;
			}
			break;
		}
		default:
		break;
	}
	return ret;
}

