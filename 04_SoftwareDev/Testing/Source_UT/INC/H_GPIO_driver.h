#ifndef H_GPIO_DRIVER_H
#define H_GPIO_DRIVER_H
/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define MKEY1_IN P6.2
#define MKEY2_IN P13.7

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/
typedef enum
{
	PIN_NAME_MKEY1 = 0,
	PIN_NAME_MKEY2,
}PIN_NAME_E;
typedef enum
{
	GPIO_LOW = 0,
	GPIO_HIGH = 1
}GPIO_LEVEL;

/***********************************************************************************************************************
Global functions
***********************************************************************************************************************/
void GPIO_input_set(PIN_NAME_E pin_name, GPIO_LEVEL pin_level);
GPIO_LEVEL GPIO_input_get(PIN_NAME_E pin_name);
#endif